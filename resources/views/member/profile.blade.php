@extends('layouts.layout')
@section('content')


<div id="page-content">
        <section class="container">
            @include('member.sub_menu' , ['name' => $name])
            <div class="row">
                <div class="col-md-9">
                    <form id="form-profile" role="form" method="post" action="?" enctype="multipart/form-data">
                    {!! Form::open(['id'=>'form-profile','files' => true]) !!}    
                        <div class="row">
                            <!--Profile Picture-->
                            <div class="col-md-3 col-sm-3">
                                <section>
                                    <h3><i class="fa fa-image"></i>Foto Profile</h3>
                                    <div id="profile-picture" class="profile-picture dropzone">
                                        <input name="file" type="file">
                                        <div class="dz-default dz-message"><span>Klik atau tarik gambar ke sini</span></div>
                                        <img src="{{ asset(null) }}assets/img/member-2.jpg" alt="">
                                    </div>
                                </section>
                            </div>
                            <!--/.col-md-3-->

                            <!--Contact Info-->
                            <div class="col-md-9 col-sm-9">
                                <section>
                                    <h3><i class="fa fa-user"></i>Identitas Diri</h3>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="name">Nama Lengkap</label>
                                                {!! Form::text('name' , $model->name , ['id' => 'name']) !!}
                                            </div>
                                            <!--/.form-group-->
                                        </div>
                                        <!--/.col-md-3-->
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                {!! Form::text('email' , $model->email , ['id' => 'email' , 'readonly' => true]) !!}
                                            </div>
                                            <!-- form-group-->
                                        </div>
                                        <!--/.col-md-3-->
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="phone">No Handphone</label>
                                                {!! Form::text('phone' , $model->phone , ['id' => 'phone']) !!}
                                            
                                            </div>
                                            <!--/.form-group-->
                                        </div>
                                        <!--/.col-md-3-->
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="pin-bb">Pin BB</label>
                                                {!! Form::text('pin_bb' , $model->phone , ['id' => 'pin_bb']) !!}
                                            
                                            </div>
                                            <!--/.form-group-->
                                        </div>
                                        <!--/.col-md-3-->
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <input type="checkbox" class="whatsapp pull-left" id="whatsapp" name="whatsapp" {{ $model->whatsapp == 'y' ? 'checked' : '' }}>
                                                <label for="whatsapp">Saya bisa dihubungi via whatsapp <img src="{{ asset(null) }}assets/img/whatsapp.png" alt="whatsapp"></label>
                                            </div>
                                            
                                            <!--/.form-group-->
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                 <button type="submit" class="btn btn-large btn-default pull-right" id="submit">Simpan</button>
                                            </div>
                                            <!--/.form-group-->
                                        </div>
                                        <!--/.col-md-3-->
                                    </div>
                                </section>
                            </div>
                            <!--/.col-md-6-->
                        </div>
                    {!! Form::close() !!}
                </div>
                <!--Password-->
                <div class="col-md-3 col-sm-9">
                    <h3><i class="fa fa-asterisk"></i>Ubah Password</h3>
                    <form class="framed" id="form-password" role="form" method="post" action="{{ url('member/change-password') }}" >
                    {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="current-password">Password Saat Ini</label>
                            <input type="password" class="form-control" id="current-password" name="old_password">
                        </div>
                        <!--/.form-group-->
                        <div class="form-group">
                            <label for="new-password">Password Baru</label>
                            <input type="password" class="form-control" id="new-password" placeholder="Minimal 8 karakter" name="password">
                        </div>
                        <!--/.form-group-->
                        <div class="form-group">
                            <label for="confirm-new-password">Ulangi Password Baru</label>
                            <input type="password" class="form-control" id="confirm-new-password" placeholder="Minimal 8 karakter" name="confirm">
                        </div>
                        <!--/.form-group-->
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Ubah Password</button>
                        </div>
                        <!-- /.form-group -->
                    </form>
                </div>
                <!-- /.col-md-3-->
            </div>
        </section>
    </div>
@endsection
@section('script')
    @include('common.error')

    @if(Session::has('success'))
       <script type="text/javascript">
           swal('success','{{ Session::get("success") }}','success');
       </script>
    @endif
@endsection