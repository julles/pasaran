@extends('layouts.layout')
@section('content')

<div id="page-content">
	<section class="container">
	    @include('member.sub_menu' , ['name' => $name])
        <div class="row">
	        <div class="col-md-3 col-sm-3">
	            <aside id="sidebar">
	                <ul class="navigation-sidebar list-unstyled">
	                    <li class="active">
	                        <a href="{{ url('member/iklan-saya/all') }}">
	                            <i class="fa fa-folder"></i>
	                            <span>Semua Iklan Saya</span>
	                        </a>
	                    </li>
	                    <li>
	                        <a href="{{ url('member/iklan-saya/aktif') }}">
	                            <i class="fa fa-check"></i>
	                            <span>Aktif</span>
	                        </a>
	                    </li>
	                    <li>
	                        <a href="{{ url('member/iklan-saya/menunggu-verifikasi') }}">
	                            <i class="fa fa-clock-o"></i>
	                            <span>Menunggu verifikasi</span>
	                        </a>
	                    </li>
	                    <li>
	                        <a href="{{ url('member/iklan-saya/tidak-aktif') }}">
	                            <i class="fa fa-eye-slash"></i>
	                            <span>Tidak Aktif</span>
	                        </a>
	                    </li>
	                </ul>
	            </aside>
	        </div>
	        <div class="col-md-9 col-sm-9">
	            <section id="items">
	            @foreach($model as $row)
	                <div class="item list admin-view">
	                    <div class="image">
	                        <a href="{{ url('item/'.$row->slug) }}">
	                            <div class="overlay">
	                                <div class="inner">
	                                    <div class="content">
	                                        <h4>Deskripsi</h4>
	                                        <p>{{ $row->deskripsi }}</p>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="item-specific">
	                                <span title="timestamp">{{ ago($row->created_at) }} ({{ Carbon\Carbon::parse($row->created_at)->format("d F , Y") }})</span>
	                            </div>
	                            <img src="{{ asset(null) }}contents/thumbnails/{{ $row->foto }}" alt="{{ $row->title }}">
	                        </a>
	                    </div>
	                    <div class="wrapper">
	                        <a href="{{ url('item/'.$row->slug) }}"><h3>{{ $row->judul }}</h3></a>
	                        <figure>Alamat</figure>
	                        <div class="info">
	                            <div class="type">
	                                <i class="fa fa-eye"></i>
	                                <span>Dilihat 1000 kali</span>
	                            </div>
	                            <div class="type">
	                                <a href="#"><i><img src="{{ asset(null) }}assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
	                                <span>{ {{ $row->category->title }} }</span></a>
	                            </div> 
	                        </div>
	                    </div>
	                    <div class="description">
	                        <ul class="list-unstyled actions">
	                            <li><a href="{{ url('member/edit-iklan/'.$row->slug) }}" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a></li>
	                            @if($row->status != 'not_approve')	
	                            	<li><a href="#" data-toggle="tooltip" title="Non-Aktifkan"><i class="fa fa-eye-slash"></i></a></li>
	                        	@endif
	                        </ul>
	                    </div>
	                    {!! icon_status($row->status) !!}
	                </div>
	            @endforeach
	            </section>
	        </div>
	    </div>
	</section>
</div>
	<!-- end Page Content-->
@endsection
