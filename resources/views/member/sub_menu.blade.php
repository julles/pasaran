
<?php
	
	$segment2 = request()->segment(2);
	$active = function($param) use ($segment2)
	{
		if($param == $segment2)
		{
			return 'active';
		}
	};
?>
<header>
    <ul class="nav nav-pills">
        <li class="{{ $active("").$active("all") }}"><a href="{{ url('member') }}"><h1 class="page-title">{{ $name }}</h1></a></li>
        <li class="{{ $active("iklan-saya") }}"><a href="{{ url('member/iklan-saya') }}"><h1 class="page-title">Iklan Saya</h1></a></li>
        <li><a href="{{ asset(null) }}mailbox.html"><h1 class="page-title">Inbox</h1></a></li>
    </ul>
</header>