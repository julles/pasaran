@extends('layouts.layout')

@section('content')
    
    <!--Page Content-->
            <div id="page-content">
                <section class="container">
                    <header>
                        <h1 class="page-title" style="text-align: center; font-size: 24px;">Pasang Iklan</h1>
                    </header>
                    <div class="row">
                        <!--Content-->
                            {!! Form::model($model,['role'=>'form','files' =>true]) !!}
                            <div class="col-md-8">
                                <section>
                                    <div class="form-group large">
                                        <label for="title">Judul *</label>
                                        {!! Form::text('judul' , null, ['class' => 'form-control']) !!}
                                    </div>
                                </section>
                                <section>
                                    <div class="form-group large">
                                        <label for="title">Harga *</label>
                                        {!! Form::text('harga' , null, ['class' => 'form-control']) !!}
                                    </div>
                                </section>
                                <!--Foto-->
                                <section>
                                    <h4>Upload Foto *</h4>
                                    {!! Form::file('foto') !!}
                                    <!--div id="file-submit" class="dropzone">
                                        <input name="file" type="file" multiple>
                                        <div class="dz-default dz-message"><span>Klik atau tarik foto ke sini</span></div>
                                    </div-->
                                </section>
                                <!--end Foto-->
                                @if(!empty($model->foto))
                                <section>
                                    <div class="form-group large">
                                   
                                        <label for="title">Old Foto</label> <br/>
                                        <img width = '200' height="200" src = "{{ asset(null) }}contents/thumbnails/{{ $model->foto }}" />
                                    </div>
                                </section>
                                @endif
                                <section>
                                    <div class="form-group large">
                                        <label for="kategori">Pilih Kategori *</label>
                                        <select class="form-control" name="iklan_category_id" dropup title="Silahkan Pilih Kategori..." data-live-search="true">
                                        <!--select  name="iklan_category_id"  title="Silahkan Pilih Kategori..."-->
                                           
                                        {!! $categories !!}
                                            <!--option value="1">Otomotif</option>
                                            <option value="2" class="sub-category">Mobil</option>
                                            <option value="3" class="sub-category-second">Sparepart</option>
                                            <option value="4" class="sub-category-third">YSS</option>
                                            <option value="5" class="sub-category">Motor</option>
                                            <option value="6" class="sub-category-second">Sparepart</option>
                                            <option value="7" class="sub-category-third">Yss</option>
                                            <option value="8">Properti</option>
                                            <option value="9" class="sub-category">Rumah</option>
                                            <option value="10" class="sub-category-second">Dijual</option>
                                            <option value="11" class="sub-category-second">Disewakan</option>
                                            <option value="12" class="sub-category">Apartement</option>
                                            <option value="13" class="sub-category-second">Dijual</option>
                                            <option value="14" class="sub-category-second">Disewakan</option-->
                                        </select>
                                    </div>
                                    <!-- /.form-group -->
                                </section>
                                <section>
                                    <div class="form-group large">
                                        <label for="title">Deskripsikan iklan anda *</label>
                                        {!! Form::textarea('deskripsi' , null, ['class' => 'form-control','style'=>'resize:vertical']) !!}
                                   
                                    </div>
                                </section>
                                <section>
                                    <div class="form-group large">
                                        <label for="title">Provinsi *</label>
                                        {!! Form::text('provinsi',null,['class' => 'form-control']) !!}
                                    </div>
                                </section>
                                <section>
                                    <div class="form-group large">
                                        <label for="title">Kota *</label>
                                        {!! Form::text('kota',null,['class' => 'form-control']) !!}
                                    </div>
                                </section>
                                <!--section>
                                    <div class="form-group">
                                        <label class="pull-left" or="title"><input type="checkbox" class="pull-left" id="map-show" name="map-show" checked>Tampilkan Iklan Saya di Peta</label>
                                        <input type="text" class="location" id="searchTextField" placeholder="Silahkan Pilih Lokasi ...">
                                        <div id="map_canvas" class = 'map_submit'></div>

                                    </div>
                                </section-->

                                <section>
                                    <div class="form-group large">
                                        <label for="title">Longitude</label>
                                        {!! Form::text('longitude' , null, ['class' => 'form-control']) !!}
                                   </div>
                                </section>

                                <section>
                                    <div class="form-group large">
                                        <label for="title">Lattitude</label>
                                        {!! Form::text('lattitude' , null, ['class' => 'form-control']) !!}
                                   </div>
                                </section>
                                <hr>
                            </div>
                            <!--/.col-md-9-->
                            <!--Informasi Pemasang-->
                            @if(!Auth::guard('member')->check())
                                <div class="col-md-4 col-sm-9">
                                    <h3><i class="fa fa-info-circle"></i> Informasi Pemasang</h3>

                                        <div class="form-group">
                                            <label for="name">Nama Lengkap</label>
                                            <input type="text" class="form-control" id="name" name="name" value="Prana Jaya">
                                        </div>
                                        <!--/.form-group-->
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" name="email" value="">
                                        </div>
                                        <!--/.form-group-->
                                        <div class="form-group">
                                            <label for="pin-bb">Pin BB <img src="assets/img/bbm.png" alt=""></label>
                                            <input type="text" class="form-control" id="pin-bb" name="pin-bb" pattern="\d*" value="021 8282828282">
                                        </div>
                                        <!--/.form-group-->
                                        <div class="form-group">
                                            <label for="phone">No Handphone</label>
                                            <input type="text" class="form-control" id="phone" name="phone" pattern="\d*" value="08383892928382">
                                        </div>
                                        <!--/.form-group-->
                                        <div class="form-group">
                                            <input type="checkbox" class="whatsapp pull-left" id="whatsapp" name="whatsapp" checked>
                                            <label for="whatsapp">Saya bisa dihubungi via whatsapp <img src="assets/img/whatsapp.png" onmousedown="return false;" alt="whatsapp"></label>
                                        </div>
                                </div>
                            @endif
                            <!--/.form-group-->
                            <div class="col-md-12">
                                <section>
                                    <div class="form-group pull-left">
                                        <input type="checkbox" class="terms pull-left" id="terms" name="terms">
                                        <label class="terms">Saya setuju untuk memproses data iklan & pribadi dengan <a href="terms-conditions.html" class="link">Syarat Penggunaan Pasaran.com</a> sesuai dengan ketentuan hukum. </label>
                                    </div>
                                    <div class="form-group pull-right">
                                        <button type="submit" class="btn btn-default pull-right" id="submit">Pasang Iklan</button>
                                    </div>
                                        <!-- /.form-group -->
                                </section>
                            </div>
                        {!! Form::close() !!}
                        <!-- /.col-md-3-->
                    </div>
                </section>
            </div>

@endsection
@section('script')
    @include('common.error')

    @if(Session::has('success'))
       <script type="text/javascript">
           swal('success','{{ Session::get("success") }}','success');
       </script>
    @endif
@endsection

@section('script_bottom')

@endsection