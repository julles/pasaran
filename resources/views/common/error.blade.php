@if(!empty($errors))
      @if($errors->any())
              <?php 
              $strError = "";
              foreach($errors->all() as $error)
              {
                  $strError .= "<br/>".$error;
              }
              ?>
              <script type="text/javascript">
                  textError = '{!! $strError !!}';
                  swal({
                    title: "Error Validation",
                    text: textError,
                    html: true,
                    type: 'error',
                  });
              </script>
      @endif
  @endif