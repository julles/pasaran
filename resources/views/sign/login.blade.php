@extends('layouts.layout')

@section('content')

<!--Page Content-->
<div id="page-content">
    <section class="container">
        <div class="block">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-md-offset-3 col-sm-offset-1">
                    <header>
                        <h1 class="page-title">Masuk ke akun Pasaran</h1>
                    </header>
                    <hr>
                    <? /* id="form-sign-in-account" */ ?>
                    <form role="form"  method="post" action="{{ url('sign/login') }}">
                    {!!  csrf_field() !!}    
                        <div class="form-group">
                            <label for="form-sign-in-email">Email:</label>
                            <input type="email" class="form-control" value = '{{ old("email") }}' id="form-sign-in-email" name="email" placeholder="Masukan Email Anda..."required>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label for="form-sign-in-password">Kata Sandi:</label>
                            <input type="password" class="form-control" id="form-sign-in-password" name="password" placeholder="Masukan Password Anda..." required>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <input type="checkbox" class="lupa-pass pull-left" id="form-ingat-saya" name="form-ingat-saya" checked>
                            <label for="form-ingat-saya">Biarkan saya tetap masuk</label>
                        </div><!-- /.form-group -->
                        <div class="form-group clearfix">
                            <span class="pull-left"><a href="#">Lupa Password ?</a></span>
                            <button type="submit" class="btn pull-right btn-default" id="account-submit">Masuk</button>
                        </div><!-- /.form-group -->
                    </form>
                </div>
                <div class="col-offset-1">
                    <div class="border"><img src="{{ asset(null) }}assets/img/border.png"></div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <header>
                        <h5 class="page-title">Belum Punya Akun ? <a href="{{ url('sign/daftar') }}">Daftar Sekarang!</a></h5>
                    </header>
                    <div class="social-login">
                        <a class="btn btn-block btn-social btn-twitter">
                              <span class="fa fa-twitter"></span>
                              Sign in with Twitter
                        </a>
                        <a class="btn btn-block btn-social btn-facebook">
                              <span class="fa fa-facebook"></span>
                              Sign in with Facebook
                        </a>
                        <a class="btn btn-block btn-social btn-google">
                              <span class="fa fa-google"></span>
                              Sign in with Google
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.block-->
</div>

@endsection