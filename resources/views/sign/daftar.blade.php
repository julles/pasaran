@extends('layouts.layout')

@section('content')

<div id="page-content">
    <section class="container">
        <div class="block">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                    <header>
                        <h1 class="page-title">Daftar</h1>
                    </header>
                    <hr>
                    {!! Form::open(['role' => 'form' , 'id' => 'form_register']) !!}
                        <div class="form-group">
                            <label for="form-register-email">Email:</label>
                            {!! Form::text('email' , null , ['class' => 'form-control' ,'id' => 'form-register-email']) !!}
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label for="form-register-password">Kata Sandi:</label>
                            {!! Form::password('password' , ['class' => 'form-control' ,'id' => 'form-register-password']) !!}
                        
                        </div><!-- /.form-group -->
                        <div class="form-group">
                            <label for="form-register-confirm-password">Ulangi Kata sandi:</label>
                            
                            {!! Form::password('confirm' , ['class' => 'form-control' ,'id' => 'form-register-confirm-password']) !!}
                        </div><!-- /.form-group -->
                        <div class="checkbox pull-left">
                            <label>
                                <input type="checkbox" name="newsletter">Terima newslater.
                            </label>
                        </div>
                        <div class="form-group clearfix">
                            <button type="submit" class="btn pull-right btn-default" id="account-submit">Daftar</button>
                        </div><!-- /.form-group -->
                    {!! Form::close() !!}
                    <hr>
                    <div class="center">
                        <figure class="note">By clicking the “Create an Account” button you agree with our <a href="terms-conditions.html" class="link">Terms and conditions</a></figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.block-->
</div>


@endsection

@section('script')
    @include('common.error')

    @if(Session::has('success'))
       <script type="text/javascript">
           swal('success','{{ Session::get("success") }}','success');
       </script>
    @endif
@endsection