@extends('oblagio.layouts.layout')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
		
				{!! Form::model($model,['files'=>true]) !!}
		        	
		    	
				<div class="box-body">

				@include('oblagio.common.error_validation')

				<div class="form-group">
				  <h2>Member</h2>
				</div>

				<div class="form-group">
				  <label>Nama</label>
				  {!! Form::text('name',$model->member->name,['class'=>'form-control','readonly' => true]) !!}
				</div>

				<div class="form-group">
				  <label>Email</label>
				  {!! Form::text('name',$model->member->email,['class'=>'form-control','readonly' => true]) !!}
				</div>

				<div class="form-group">
				  <label>Tanggal Bergabung</label>
				  {!! Form::text('tanggal_bergabung',Carbon\Carbon::parse($model->member->created_at)->format("d M Y , H:i:s"),['class'=>'form-control','readonly' => true]) !!}
				</div>

				</div><!-- /.box-body -->
				{!! Form::close() !!}

		</div>
		<div class="box box-primary">
		
				{!! Form::model($model,['files'=>true]) !!}
		        	
		    	
				<div class="box-body">

				@include('oblagio.common.error_validation')

				<div class="form-group">
				  <h2>Iklan</h2>
				</div>

				<div class="form-group">
				  <label>Category</label>
				  {!! Form::text('category_iklan_id',$model->category->title,['class'=>'form-control','readonly' => true]) !!}
				</div>

				<div class="form-group">
				  <label>Judul</label>
				  {!! Form::text('judul',null,['class'=>'form-control','readonly' => true]) !!}
				</div>

				<div class="form-group">
				  <label>Deskripsi</label>
				  {!! Form::text('deskripsi',null,['class'=>'form-control','readonly' => true]) !!}
				</div>

				<div class="form-group">
				  <label>Provinsi</label>
				  {!! Form::text('provinsi',null,['class'=>'form-control','readonly' => true]) !!}
				</div>

				<div class="form-group">
				  <label>Kota</label>
				  {!! Form::text('kota',null,['class'=>'form-control','readonly' => true]) !!}
				</div>

				<div class="form-group">
				  <label>Image</label> <br/>
				  <img src = "{{ asset('contents/'.$model->foto) }}" width = "200" height = "200" />
				</div>				

				<div class="form-group">
				  <label>Tanggal Posting</label>
				  {!! Form::text('created_at',Carbon\Carbon::parse($model->created_at)->format("d M Y , H:i:s"),['class'=>'form-control','readonly' => true]) !!}
				</div>

				<div class="form-group">
				  <label>Status</label>
				  {!! Form::select('status',['not_approve' => 'Pending','active' => 'Active','in_active' => 'Not Active'],null,['class'=>'form-control']) !!}
				</div>

				</div><!-- /.box-body -->
				<div class="box-footer">
		            <button class="btn btn-primary" type="submit">Update Status</button>
		        </div>
				{!! Form::close() !!}

		</div>	 	  
	</div>
</div>
    

@endsection

