@extends('oblagio.layouts.layout')
@section('content')
<div class="row">
	<div class="col-md-12">

		<div style="margin-bottom:10px;">
			<a href = "{{ og()->urlBackendAction('index/active') }}" class = 'btn btn-success'>Active</a>
			<a href = "{{ og()->urlBackendAction('index/in_active') }}" class = 'btn btn-danger'>Not Active</a>
			<a href = "{{ og()->urlBackendAction('index/not_approve') }}" class = 'btn btn-warning'>Pending</a>
		</div>

	  <div class="box">
	    <div class="box-body">
	    
	    {!! og()->linkCreate() !!}
	    
	      	<hr/>
	      	@include('oblagio.common.all_flashes')

	      		<table class="table table-bordered" id="table">
			        <thead>
			            <tr>
			            	<th width="">Member</th>
			                <th width="">Judul Iklan</th>
			                <th width="">Status</th>
			                <th width="">Action</th>
			            </tr>
			        </thead>
			    </table>
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->

	  
	</div><!-- /.col -->
    
 </div>

@endsection

@section('script')

<script type="text/javascript">
	$(function() {
	$.fn.dataTable.ext.errMode = 'none';
    $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{og()->urlBackendAction("data/".$status)}}',
        columns: [
            { data: 'name', name: 'name' },
            { data: 'judul', name: 'judul' },
            { data: 'status', name: 'status',"searchable": false ,'orderable' : false },
            { data: 'action', name: 'action',"searchable": false ,'orderable' : false },
        ]
    });
});
</script>

@endsection