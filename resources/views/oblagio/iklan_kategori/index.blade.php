@extends('oblagio.layouts.layout')
@section('content')

<?php
	
	function childs($modelParent,$number)
	{
		$str = "";

		$strip = function($no){
			$str="";
			for($i=0;$i<$no;$i++)
			{
				$str.='-';
			}

			return $str;
		};

		$no = $number + 1;

		foreach($modelParent->childs as $row)
		{
			$no++;

			
			$str.='<tr>
					<td>&nbsp;</td>
					<td>'.$strip($no).$row->title.'</td>
					<td>'.og()->links($row->id).'</td>
				  </tr>';

			$str .= childs($row,$no + 1);

			
		}
		return $str;
	}

	
?>

<div class="row">
	<div class="col-md-12">
	  <div class="box">
	    <div class="box-body">
	    
	    {!! og()->linkCreate() !!}
	    
	      	<hr/>
	      	@include('oblagio.common.all_flashes')
	      	<table class="table table-bordered" id="table">
		        <thead>
		            <tr>
		            	<th width="20%">No</th>
		                <th width="30%">Title</th>
		                <th width="20%">Action</th>
		            </tr>
		        </thead>
		        <tbody>

		        <?php
		        $no=0;
		        ?>

		        @foreach($model->whereParentId(0)->get() as $parent)
		       	<?php $no++; ?>
		        	<tr>
		        		<td>{{ $no }}</td>
			        	<td>{{ $parent->title }}</td>
			        	<td>{!! og()->links($parent->id) !!}</td>
			        </tr>

			        {!! childs($parent,$no) !!}

			    @endforeach
		        </tbody>
		    </table>
	    </div><!-- /.box-body -->
	  </div><!-- /.box -->

	  
	</div><!-- /.col -->
    
 </div>

@endsection

@section('script')

<script type="text/javascript">
	$(function() {
	$.fn.dataTable.ext.errMode = 'none';
    $('#table').DataTable({
    	ordering:false,
    });
});
</script>

@endsection