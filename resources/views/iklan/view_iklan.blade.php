@extends('layouts.layout')

@section('content')
    
    <!--Page Content-->
            <div id="page-content">

                <div id="map-detail"></div>
                <section class="container">
                    <div class="row">
                        <!--Item Detail Content-->
                        <div class="col-md-9">
                            <section class="block" id="main-content">
                                <header class="page-title">
                                    <div class="title">
                                        <h1>{{ $model->judul }}</h1>
                                        <figure>{{ $model->provinsi }} {{ $model->kota }}</figure>
                                    </div>
                                    <!--div class="fav">
                                        <div class="type">
                                            <a href="#" data-toogle="tooltip" title="Suka"><i class="fa fa-thumbs-up fa-lg"></i></a>
                                        </div>
                                    </div-->
                                </header>
                                <div class="row">
                                    <!--Detail Sidebar-->
                                    <aside class="col-md-4 col-sm-4" id="detail-sidebar">
                                        <section>
                                            <header><h3><div class="btn_price">Rp. {{ og()->money($model->harga) }}</div></h3></header>
                                        </section>
                                        <!--Contact-->
                                        <section class="box-white">
                                            <header><h3>{{ ucwords($model->member->name) }}</h3></header>
                                            <address>
                                                <div>Member sejak {{ Carbon\Carbon::parse($model->member->created_at)->format("F Y") }} </div>
                                                <div>Terakhir login <b>{{ $model->member->lastLogin() }}</b></div>
                                                <figure>
                                                    <div class="info">
                                                        <img src="{{ asset(null) }}assets/img/whatsapp.png" alt="whatsapp">
                                                        <span><b> {{ $model->member->whatsapp == 'y' ? $model->member->phone : '' }}</b></span>
                                                    </div>
                                                    <div class="info">
                                                        <img src="{{ asset(null) }}assets/img/bbm.png" alt="bbm">
                                                        <span><b> {{ $model->member->pin_bb }}</b></span>
                                                    </div>
                                                </figure>
                                            </address>
                                        </section>
                                        <!--Pesan-->
                                        <section class="box-white">
                                            <header><h3>Kontak Ke Penjual</h3></header>
                                            <figure>
                                                <form id="item-detail-form" role="form" method="post" action="?">
                                                    <div class="form-group">
                                                        <label for="item-detail-name">Nama</label>
                                                        <input type="text" class="form-control framed" id="item-detail-name" name="item-detail-name" placeholder="Isi Nama..." required="">
                                                    </div>
                                                    <!-- /.form-group -->
                                                    <div class="form-group">
                                                        <label for="item-detail-email">Email</label>
                                                        <input type="email" class="form-control framed" id="item-detail-email" name="item-detail-email" placeholder="Isi Email..." required="">
                                                    </div>
                                                    <!-- /.form-group -->
                                                    <div class="form-group">
                                                        <label for="item-detail-message">Pesan</label>
                                                        <textarea class="form-control framed" id="item-detail-message" name="item-detail-message"  rows="3" placeholder="Isi Pesan..." required=""></textarea>
                                                    </div>
                                                    <!-- /.form-group -->
                                                    <div class="form-group">
                                                        <button type="submit" class="btn framed icon">Kirim<i class="fa fa-angle-right"></i></button>
                                                    </div>
                                                    <!-- /.form-group -->
                                                </form>
                                            </figure>
                                        </section>
                                        <!--end Pesan-->
                                    </aside>
                                    <!--end Detail Sidebar-->
                                    <!--Content-->
                                    <div class="col-md-8 col-sm-8">
                                        <section>
                                            <article class="item-gallery">
                                                <div class="owl-carousel item-slider">
                                                    <div class="slide"><img src="{{ asset('contents/'.$model->foto) }}" data-hash="1" alt=""></div>
                                                    <!--div class="slide"><img src="{{ asset(null) }}assets/img/items/2.jpg" data-hash="2" alt=""></div>
                                                    <div class="slide"><img src="{{ asset(null) }}assets/img/items/3.jpg" data-hash="3" alt=""></div>
                                                    <div class="slide"><img src="{{ asset(null) }}assets/img/items/4.jpg" data-hash="4" alt=""></div>
                                                    <div class="slide"><img src="{{ asset(null) }}assets/img/items/5.jpg" data-hash="5" alt=""></div>
                                                    <div class="slide"><img src="{{ asset(null) }}assets/img/items/6.jpg" data-hash="6" alt=""></div>
                                                    <div class="slide"><img src="{{ asset(null) }}assets/img/items/7.jpg" data-hash="7" alt=""></div-->
                                                </div>
                                                <!-- /.item-slider -->
                                                <div class="thumbnails">
                                                    <span class="expand-content btn framed icon" data-expand="#gallery-thumbnails" ><i class="no-margin fa fa-plus"></i></span>
                                                    <div class="expandable-content height collapsed show-70" id="gallery-thumbnails">
                                                        <div class="content">
                                                            <a href="#1" id="thumbnail-1" class="active"><img src="{{ asset('contents/'.$model->foto) }}" alt=""></a>
                                                            <!--a href="#2" id="thumbnail-2"><img src="{{ asset(null) }}assets/img/items/2.jpg" alt=""></a>
                                                            <a href="#3" id="thumbnail-3"><img src="{{ asset(null) }}assets/img/items/3.jpg" alt=""></a>
                                                            <a href="#4" id="thumbnail-4"><img src="{{ asset(null) }}assets/img/items/4.jpg" alt=""></a>
                                                            <a href="#5" id="thumbnail-5"><img src="{{ asset(null) }}assets/img/items/5.jpg" alt=""></a>
                                                            <a href="#6" id="thumbnail-6"><img src="{{ asset(null) }}assets/img/items/6.jpg" alt=""></a>
                                                            <a href="#7" id="thumbnail-7"><img src="{{ asset(null) }}assets/img/items/7.jpg" alt=""></a-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                            <!-- /.item-gallery -->
                                            <article class="block">
                                                <header><i class="fa fa-eye pull-right"> {{ $model->views()->count() }} kali </i><h2>Deskripsi</h2></header>
                                                <p>
                                                    {!! $model->deskripsi !!}
                                                </p>
                                            </article>
                                            <!-- /.block -->
                                        </section>
                                        <section>
                                            <h3><a href="#"><i class="fa fa-envelope"> Kirim Ke Teman </i></a>
                                            <a href="#" class="pull-right"><i class="fa fa-exclamation-circle"> Laporkan Penjual</i></a></h3>
                                        </section>
                                    </div>
                                    <!-- /.col-md-8-->
                                </div>
                                <!-- /.row -->
                            </section>
                            <!-- /#main-content-->
                        </div>
                        <!-- /.col-md-8-->
                        <!--Sidebar-->
                        <div class="col-md-3">
                            <aside id="sidebar">
                                 <!--Sharing-->
                                <section class="box-white-left clearfix">
                                        <ul class="social-nav sharing pull-right">
                                            <li>Bagikan</li>
                                            <li><a href="#" class="twitter" data-toggle="tooltip" title="Bagikan di Twitter"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="#" class="facebook" data-toggle="tooltip" title="Bagikan di Facebook"> <i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#" class="google-plus" data-toggle="tooltip" title="Bagikan di Google +"><i class="fa fa-google-plus"></i></a></li>
                                        </ul>
                                </section>
                                        <!--end Sharing-->
                                <section>
                                    <h2>Iklan Terkait</h2>
                                    <a href="item-detail.html" class="item-horizontal small">
                                        <h3>Cash Cow Restaurante</h3>
                                        <figure>63 Birch Street</figure>
                                        <div class="wrapper">
                                            <div class="image"><img src="{{ asset(null) }}assets/img/items/1.jpg" alt=""></div>
                                            <div class="info">
                                                <div class="type">
                                                    <i><img src="{{ asset(null) }}assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                    <span>Restaurant</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <!--/.item-horizontal small-->
                                    <a href="item-detail.html" class="item-horizontal small">
                                        <h3>Blue Chilli</h3>
                                        <figure>2476 Whispering Pines Circle</figure>
                                        <div class="wrapper">
                                            <div class="image"><img src="{{ asset(null) }}assets/img/items/2.jpg" alt=""></div>
                                            <div class="info">
                                                <div class="type">
                                                    <i><img src="{{ asset(null) }}assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                    <span>Restaurant</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <!--/.item-horizontal small-->
                                    <a href="item-detail.html" class="item-horizontal small">
                                        <h3>Eddie’s Fast Food</h3>
                                        <figure>4365 Bruce Street</figure>
                                        <div class="wrapper">
                                            <div class="image"><img src="{{ asset(null) }}assets/img/items/3.jpg" alt=""></div>
                                            <div class="info">
                                                <div class="type">
                                                    <i><img src="{{ asset(null) }}assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                    <span>Restaurant</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <!--/.item-horizontal small-->
                                    <a href="item-detail.html" class="item-horizontal small">
                                        <h3>Eddie’s Fast Food</h3>
                                        <figure>4365 Bruce Street</figure>
                                        <div class="wrapper">
                                            <div class="image"><img src="{{ asset(null) }}assets/img/items/3.jpg" alt=""></div>
                                            <div class="info">
                                                <div class="type">
                                                    <i><img src="{{ asset(null) }}assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                    <span>Restaurant</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <!--/.item-horizontal small-->
                                    <a href="item-detail.html" class="item-horizontal small">
                                        <h3>Eddie’s Fast Food</h3>
                                        <figure>4365 Bruce Street</figure>
                                        <div class="wrapper">
                                            <div class="image"><img src="{{ asset(null) }}assets/img/items/3.jpg" alt=""></div>
                                            <div class="info">
                                                <div class="type">
                                                    <i><img src="{{ asset(null) }}assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                    <span>Restaurant</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <!--/.item-horizontal small-->
                                </section>
                            </aside>
                            <!-- /#sidebar-->
                        </div>
                        <!-- /.col-md-3-->
                        <!--end Sidebar-->
                    </div><!-- /.row-->
                </section>
                <!-- /.container-->
            </div>
            <!-- end Page Content-->

@endsection
@section('script')
    @include('common.error')

    @if(Session::has('success'))
       <script type="text/javascript">
           swal('success','{{ Session::get("success") }}','success');
       </script>
    @endif
@endsection

@section('script_bottom')
    <script type="text/javascript">
        var itemID = 1;
            $.getJSON('assets/json/items.json.txt')
                .done(function(json) {
                        $.each(json.data, function(a) {
                            if( json.data[a].id == itemID ) {
                                itemDetailMap(json.data[a]);
                            }
                        });
                })
                .fail(function( jqxhr, textStatus, error ) {
                    console.log(error);
                })
            ;
            $(window).load(function(){
                var rtl = false; // Use RTL
                initializeOwl(rtl);
            });
    </script>
@endsection