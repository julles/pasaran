@extends('layouts.layout_home')
@section('content')
@include('layouts.menu')
<div id="page-content">
    <!--Hero Image-->
    <section class="hero-image search-filter-top height-500">
        <div class="inner">
            <div class="container">
                <h2>Cari kebutuhan apapun di manapun</h2>
                <div class="container" style="width: 80%; margin: 0px auto;" align="center">
                    <div class="search-bar horizontal" align="center">
                        <form class="main-search border-less-inputs" role="form" method="post">
                            <div class="input-row">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="keyword" placeholder="Cari Kata Kunci : Kursi, Rumah, Mobil dll">
                                </div>
                                <div class="form-group">
				                    <div class="input-group location">
				                        <input type="text" class="form-control" id="location" placeholder="Silahkan Pilih Lokasi ...">
				                            <span class="input-group-addon"><i class="fa fa-map-marker geolocation" data-toggle="tooltip" data-placement="bottom" title="Lokasi Saya Sekarang"></i></span>
				                    </div>
				                </div>
				                <!-- /.form-group -->
                                <div class="form-group">
                                    <a href="listing-grid.html"><button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button></a>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <!-- /.input-row -->
                        </form>
                        <!-- /.main-search -->
       				</div>
       			 <!-- /.search-bar -->
       			 </div>
            </div>
        </div>
        <div class="background">
            <img src="assets/img/pasaran-bg.png" alt="Pasaran.com">
        </div>
    </section>
    <!--end Hero Image-->

    <!--Featured-->
    <section id="featured" class="block equal-height">
        <div class="container">
            <header><h2>Terbaru</h2></header>
            <div class="row">
                @foreach($terbaru as $baru)
                        <div class="col-md-3 col-sm-3">
                            <div class="item">
                                <div class="image">
                                    <div class="quick-view"><i class="fa fa-eye"></i><span>Dilihat 1000 kali</span></div>
                                    <a href="{{ url('iklan/'.str_slug($baru->member->name).'/'.$baru->slug) }}">
                                        <div class="overlay">
                                            <div class="inner">
                                                <div class="content">
                                                    <h4>Deskripsi</h4>
                                                    <p>{{ substr($baru->deskripsi,0,100) }}. . .</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-specific">
                                            <span>2 Menit Lalu</span>
                                        </div>
                                        <div class="icon">
                                            <i class="fa fa-thumbs-up"></i>
                                        </div>
                                        <img src="{{ asset('contents/'.$baru->foto) }}" alt="">
                                    </a>
                                </div>
                                <div class="wrapper">
                                    <a href="{{ url('iklan/'.str_slug($baru->member->name).'/'.$baru->slug) }}"><h3>{{ $baru->title }}</h3></a>
                                    <figure><i class="fa fa-map-marker"></i> {{ $baru->kota }}</figure>
                                    <div class="price">Rp. {{ og()->money($baru->harga) }}</div>
                                    <div class="info">
                                        <div class="type">
                                            <!--i><img src="assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i-->
                                            <span>Restaurant</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.item-->
                        </div>
                        <!--/.col-sm-4-->
                @endforeach       
                <!--/.col-sm-4-->
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
        <div class="background opacity-5">
            <img src="assets/img/restaurants-bg2.jpg" alt="">
        </div>
    </section>
    <!--end Featured-->

    <!--Popular-->
    <section id="popular" class="block background-color-white">
        <div class="container">
            <header><h2>Populer</h2></header>
            <div class="owl-carousel wide carousel">
                @foreach($populer as $baru)
                <div class="slide">
                    <div class="inner">
                        <div class="image">
                            <img src="assets/img/items/restaurant/8.jpg">
                        </div>
                        <div class="wrapper">
                                <div class="fav pull-right">
                                    <div class="type">
                                        <a href="#"><i class="fa fa-thumbs-up fa-lg"></i></a>
                                    </div>
                                </div>
                            <a href="item-detail.html"><h3>Magma Bar and Grill</h3></a>
                            <figure>
                                <i class="fa fa-map-marker"></i>
                                <span>970 Chapel Street, Rosenberg, TX 77471</span>
                            </figure>
                            <div class="info">
                            	<div class="price">Rp. 2.000.000</div>
                                <div class="type pull-right">
                                    <i><img src="assets/icons/restaurants-bars/restaurants/restaurant.png"></i>
                                    <span>Restaurant</span>
                                </div>
                            </div>
                            <!--/.info-->
                            <p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa, viverra interdum eros ut,
                                imperdiet pellentesque mauris. Proin sit amet scelerisque risus. Donec semper semper erat ut mollis.
                                Curabitur suscipit, justo eu dignissim lacinia, ante sapien pharetra dui...
                            </p>
                            <a href="item-detail.html" class="read-more icon-readmore">Read More</a>
                        </div>
                        <!--/.wrapper-->
                    </div>
                    <!--/.inner-->
                </div>
                @endforeach
                
                <!--/.slide-->
            </div>
            <!--/.owl-carousel-->
        </div>
        <!--/.container-->
    </section>
    <!--end Popular-->

    <!--Why Us-->
   <section id="why-us" class="block">
       <div class="container">
           <header><h2>Why Us?</h2></header>
           <div class="row">
               <div class="col-md-4 col-sm-4">
                   <div class="feature-box">
                       <i class="fa fa-search red"></i>
                       <div class="description">
                           <h3>Pencarian Lengkap</h3>
                           <p>
                               Praesent tempor a erat in iaculis. Phasellus vitae libero libero. Vestibulum ante
                               ipsum primis in faucibus orci luctus et ultrices posuere cubilia
                           </p>
                       </div>
                   </div>
                   <!--/.feature-box-->
               </div>
               <!--/.col-md-4-->
               <div class="col-md-4 col-sm-4">
                   <div class="feature-box">
                       <i class="fa fa-map green"></i>
                       <div class="description">
                           <h3>Pencarian Map</h3>
                           <p>
                               Pellentesque nisl quam, aliquet sed velit eu, varius condimentum nunc.
                               Nunc vulputate turpis ut erat egestas, vitae rutrum sapien varius.
                           </p>
                       </div>
                   </div>
                   <!--/.feature-box-->
               </div>
               <!--/.col-md-4-->
               <div class="col-md-4 col-sm-4">
                   <div class="feature-box">
                       <i class="fa fa-money blue"></i>
                       <div class="description">
                           <h3>Pemasangan Iklan Gratis</h3>
                           <p>
                               Maecenas quis ipsum lectus. Fusce molestie, metus ut consequat pulvinar,
                               ipsum quam condimentum leo, sit amet auctor lacus nulla at felis.
                           </p>
                       </div>
                   </div>
                   <!--/.feature-box-->
               </div>
               <!--/.col-md-4-->
           </div>
       </div>
   </section>
   <!--end Why Us-->

    <hr>

    <!--Listing-->
    <section id="listing" class="block">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-9">
                        <header><h2>Populer Hari Ini</h2></header>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="item ">
                                    <div class="image">
                                        <div class="quick-view"><i class="fa fa-eye"></i><span>Dilihat 1000 kali</span></div>
                                        <a href="item-detail.html">
                                            <div class="overlay">
                                                <div class="inner">
                                                    <div class="content">
                                                        <h4>Deskripsi</h4>
                                                        <p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-specific">
                                                <span>Hari Ini</span>
                                            </div>
                                            <div class="icon">
                                                <i class="fa fa-thumbs-up"></i>
                                            </div>
                                            <img src="assets/img/items/restaurant/11.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="wrapper">
                                        <a href="item-detail.html"><h3>Max Five Lounge</h3></a>
                                        <figure><i class="fa fa-map-marker"></i> DKI Jakarta</figure>
                                        <div class="price">Rp. 2.000.000</div>
                                        <div class="info">
                                            <div class="type">
                                                <i><img src="assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                <span>Restaurant</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.item-->
                            </div>
                            <!--/.col-sm-4-->
                            <div class="col-md-4 col-sm-4">
                                <div class="item ">
                                    <div class="image">
                                        <div class="quick-view"><i class="fa fa-eye"></i><span>Dilihat 1000 kali</span></div>
                                        <a href="item-detail.html">
                                            <div class="overlay">
                                                <div class="inner">
                                                    <div class="content">
                                                        <h4>Deskripsi</h4>
                                                        <p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-specific">
                                                <span>Hari Ini</span>
                                            </div>
                                            <div class="icon icon-on">
                                                <i class="fa fa-thumbs-up"></i>
                                            </div>
                                            <img src="assets/img/items/restaurant/4.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="wrapper">
                                        <a href="item-detail.html"><h3>Magma Bar & Grill</h3></a>
                                        <figure><i class="fa fa-map-marker"></i> DKI Jakarta</figure>
                                        <div class="price">Rp. 2.000.000</div>
                                        <div class="info">
                                            <div class="type">
                                                <i><img src="assets/icons/restaurants-bars/restaurants/restaurant_steakhouse.png" alt=""></i>
                                                <span>Steak House</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.item-->
                            </div>
                            <!--/.col-sm-4-->
                            <div class="col-md-4 col-sm-4">
                                <div class="item ">
                                    <div class="image">
                                        <div class="quick-view"><i class="fa fa-eye"></i><span>Dilihat 1000 kali</span></div>
                                        <a href="item-detail.html">
                                            <div class="overlay">
                                                <div class="inner">
                                                    <div class="content">
                                                        <h4>Deskripsi</h4>
                                                        <p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-specific">
                                                <span>Hari Ini</span>
                                            </div>
                                            <div class="icon">
                                                <i class="fa fa-thumbs-up"></i>
                                            </div>
                                            <img src="assets/img/items/restaurant/8.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="wrapper">
                                        <a href="item-detail.html"><h3>Guild Trattoria</h3></a>
                                        <figure><i class="fa fa-map-marker"></i> DKI Jakarta</figure>
                                        <div class="price">Rp. 2.000.000</div>
                                        <div class="info">
                                            <div class="type">
                                                <i><img src="assets/icons/restaurants-bars/restaurants/barbecue.png" alt=""></i>
                                                <span>BBQ Restaurant</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.item-->
                            </div>
                            <!--/.col-sm-4-->

                            <div class="col-md-4 col-sm-4">
                                <div class="item ">
                                    <div class="image">
                                        <div class="quick-view"><i class="fa fa-eye"></i><span>Dilihat 1000 kali</span></div>
                                        <a href="item-detail.html">
                                            <div class="overlay">
                                                <div class="inner">
                                                    <div class="content">
                                                        <h4>Deskripsi</h4>
                                                        <p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-specific">
                                                <span>Hari Ini</span>
                                            </div>
                                            <div class="icon">
                                                <i class="fa fa-thumbs-up"></i>
                                            </div>
                                            <img src="assets/img/items/restaurant/12.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="wrapper">
                                        <a href="item-detail.html"><h3>Sushi Wooshi Bar</h3></a>
                                        <figure><i class="fa fa-map-marker"></i> DKI Jakarta</figure>
                                        <div class="price">Rp. 2.000.000</div>
                                        <div class="info">
                                            <div class="type">
                                                <i><img src="assets/icons/restaurants-bars/restaurants/fishchips.png" alt=""></i>
                                                <span>Sushi Bar</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.item-->
                            </div>
                            <!--/.col-sm-4-->
                            <div class="col-md-4 col-sm-4">
                                <div class="item ">
                                    <div class="image">
                                        <div class="quick-view"><i class="fa fa-eye"></i><span>Dilihat 1000 kali</span></div>
                                        <a href="item-detail.html">
                                            <div class="overlay">
                                                <div class="inner">
                                                    <div class="content">
                                                        <h4>Deskripsi</h4>
                                                        <p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-specific">
                                                <span>Hari Ini</span>
                                            </div>
                                            <div class="icon">
                                                <i class="fa fa-thumbs-up"></i>
                                            </div>
                                            <img src="assets/img/items/4.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="wrapper">
                                        <a href="item-detail.html"><h3>Max Five Lounge</h3></a>
                                        <figure><i class="fa fa-map-marker"></i> DKI Jakarta</figure>
                                        <div class="price">Rp. 2.000.000</div>
                                        <div class="info">
                                            <div class="type">
                                                <i><img src="assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                                <span>Restaurant</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.item-->
                            </div>
                            <!--/.col-sm-4-->
                            <div class="col-md-4 col-sm-4">
                                <div class="item ">
                                    <div class="image">
                                        <div class="quick-view" id="10"><i class="fa fa-eye"></i><span>Dilihat 1000 kali</span></div>
                                        <a href="item-detail.html">
                                            <div class="overlay">
                                                <div class="inner">
                                                    <div class="content">
                                                        <h4>Deskripsi</h4>
                                                        <p>Curabitur odio nibh, luctus non pulvinar a, ultricies ac diam. Donec neque massa</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-specific">
                                                <span>Hari Ini</span>
                                            </div>
                                            <div class="icon">
                                                <i class="fa fa-thumbs-up"></i>
                                            </div>
                                            <img src="assets/img/items/6.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="wrapper">
                                        <a href="item-detail.html"><h3>Magma Bar & Grill</h3></a>
                                        <figure><i class="fa fa-map-marker"></i> DKI Jakarta</figure>
                                        <div class="price">Rp. 2.000.000</div>
                                        <div class="info">
                                            <div class="type">
                                                <i><img src="assets/icons/restaurants-bars/restaurants/restaurant_steakhouse.png" alt=""></i>
                                                <span>Steak House</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.item-->
                            </div>
                            <!--/.col-sm-4-->
                        </div>
                        <!--/.row-->
                </div>
                <div class="col-md-3 col-sm-3">
                    <aside id="sidebar">
                        <section>
                            <header><h2>Iklan Premium</h2></header>
                            <a href="item-detail.html" class="item-horizontal small">
                            	<div class="icon icon-on pull-right">
                                    <i class="fa fa-thumbs-up"></i>
                                </div>
                                <b><h3>Honda Civic thn 2009</h3></b>
                                <figure><i class="fa fa-map-marker"></i> Jakarta Barat</figure>
                                <div class="wrapper">
                                    <div class="image"><img src="assets/img/items/1.jpg" alt=""></div>
                                    <div class="info">
                                    	<div class="price"> 
                                            <span>Rp. 2.000.000</span>
                                        </div>
                                        <div class="type">
                                            <i><img src="assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                            <span>Restaurant</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <!--/.item-horizontal small-->
                            <a href="item-detail.html" class="item-horizontal small">
                            	<div class="icon icon-off pull-right">
                                    <i class="fa fa-thumbs-up"></i>
                                </div>
                                <b><h3>Ninja 250 RR thn 2011</h3></b>
                                <figure><i class="fa fa-map-marker"></i> Jakarta Selatan</figure>
                                <div class="wrapper">
                                    <div class="image"><img src="assets/img/items/1.jpg" alt=""></div>
                                    <div class="info">
                                    	<div class="price"> 
                                            <span>Rp. 2.000.000</span>
                                        </div>
                                        <div class="type">
                                            <i><img src="assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                            <span>Restaurant</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <!--/.item-horizontal small-->
                            <a href="item-detail.html" class="item-horizontal small">
                            	<div class="icon icon-on pull-right">
                                    <i class="fa fa-thumbs-up"></i>
                                </div>
                                <b><h3>Yamaha R6 thn 2014 mulus</h3></b>
                                <figure><i class="fa fa-map-marker"></i> Jakarta Pusat</figure>
                                <div class="wrapper">
                                    <div class="image"><img src="assets/img/items/1.jpg" alt=""></div>
                                    <div class="info">
                                    	<div class="price"> 
                                            <span>Rp. 2.000.000</span>
                                        </div>
                                        <div class="type">
                                            <i><img src="assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                            <span>Restaurant</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <!--/.item-horizontal small-->
                            <a href="item-detail.html" class="item-horizontal small">
                            	<div class="icon icon-off pull-right">
                                    <i class="fa fa-thumbs-up"></i>
                                </div>
                                <b><h3>CBR 250 thn 2012</h3></b>
                                <figure><i class="fa fa-map-marker"></i> Jakarta Utara</figure>
                                <div class="wrapper">
                                    <div class="image"><img src="assets/img/items/1.jpg" alt=""></div>
                                    <div class="info">
                                    	<div class="price"> 
                                            <span>Rp. 2.000.000</span>
                                        </div>
                                        <div class="type">
                                            <i><img src="assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                            <span>Restaurant</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <!--/.item-horizontal small-->
                            <a href="item-detail.html" class="item-horizontal small">
                            	<div class="icon icon-off pull-right">
                                    <i class="fa fa-thumbs-up"></i>
                                </div>
                                <h3>Yamaha R15 thn 2015</h3>
                                <figure><i class="fa fa-map-marker"></i> Bandung</figure>
                                <div class="wrapper">
                                    <div class="image"><img src="assets/img/items/1.jpg" alt=""></div>
                                    <div class="info">
                                    	<div class="price"> 
                                            <span>Rp. 2.000.000</span>
                                        </div>
                                        <div class="type">
                                            <i><img src="assets/icons/restaurants-bars/restaurants/restaurant.png" alt=""></i>
                                            <span>Restaurant</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <!--/.item-horizontal small-->
                        </section>
                    </aside>
                    <!--/#sidebar-->
                </div>
                <!--/.col-md-3-->
            </div>
            <hr>
            <!--/.row-->
        </div>
        <!--/.block-->
    </section>
    <!--end Listing-->
</div>
@endsection