<div class="header">
    <div class="wrapper">
        <nav class="navigation-items-left">
            <div class="wrapper">
                <ul class="main-navigation navigation-top-header"></ul>
                <nav class="main-nav clearfix">
                    <span class="a">
                        <i class="fa fa-shopping-cart"></i>
                        Belanja
                    </span>
                    <span class="a properti">
                        <i class="fa fa-building"></i>
                        Properti
                    </span>
                    <span class="a otomotif">
                        <i class="fa fa-car"></i>
                        Otomotif
                    </span>
                    <span class="a jasa">
                        <i class="fa fa-wrench"></i>
                        Jasa
                    </span>
                    <span class="a pekerjaan">
                        <i class="fa fa-briefcase"></i>
                        Pekerjaan
                    </span>
                    <span class="a event">
                        <i class="fa fa-users"></i>
                        Event
                    </span>
                    <span class="a iklan">
                    		<div class="icon-iklan">
                        		<a href="submit.html" style="color: #ffffff;"><i class="fa fa-plus"></i>Pasang Iklan </a>
                   			</div>
                    </span>

                </nav>
<!--                    <div class="toggle-navigation">
                    <div class="icon">
                        <div class="line"></div>
                        <div class="line"></div>
                        <div class="line"></div>
                    </div>
                </div> -->
            </div>
        </nav>
    </div>
</div>