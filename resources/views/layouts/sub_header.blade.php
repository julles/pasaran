<section class="sub-header">
                    <div id="redefine-search-form" class="search-bar horizontal collapse in">


    <title></title>



<div class="content">
    <div class="container">
        <form action="?" method="post" role="form" class="main-search inputs-3">
            <div class="input-row">
                <div class="form-group">
                    <input type="text" placeholder="Cari Kata Kunci : Kursi, Rumah, Mobil dll" id="keyword" class="form-control">
                </div><!-- /.form-group -->
                <div class="form-group">
                    <div class="input-group location">
                        <input type="text" placeholder="Silahkan Pilih Lokasi ..." id="location" class="form-control">
                            <span class="input-group-addon"><i title="Lokasi Saya Sekarang" data-placement="bottom" data-toggle="tooltip" class="fa fa-map-marker geolocation"></i></span>
                    </div>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                    <select data-live-search="true" title="Silahkan Pilih Kategori..." multiple="" name="category" style="display: none;">
                        <option value="1">Stores</option>
                        <option class="sub-category" value="2">Apparel</option>
                        <option class="sub-category" value="3">Computers</option>
                        <option class="sub-category" value="4">Nature</option>
                        <option value="5">Tourism</option>
                        <option value="6">Restaurant &amp; Bars</option>
                        <option class="sub-category" value="7">Bars</option>
                        <option class="sub-category" value="8">Vegetarian</option>
                        <option class="sub-category" value="9">Steak &amp; Grill</option>
                        <option value="10">Sports</option>
                        <option class="sub-category" value="11">Cycling</option>
                        <option class="sub-category" value="12">Water Sports</option>
                        <option class="sub-category" value="13">Winter Sports</option>
                    </select><div class="btn-group bootstrap-select show-tick"><button data-toggle="dropdown" class="btn dropdown-toggle selectpicker btn-default" type="button" title="Silahkan Pilih Kategori..."><span class="filter-option pull-left">Silahkan Pilih Kategori...</span>&nbsp;<span class="caret"></span></button><div class="dropdown-menu open"><div class="bootstrap-select-searchbox"><input type="text" class="input-block-level form-control"></div><ul role="menu" class="dropdown-menu inner selectpicker"><li rel="0"><a style="" class="" tabindex="0"><span class="text">Stores</span><i class="glyphicon glyphicon-ok icon-ok check-mark"></i></a></li><li rel="1"><a style="" class="sub-category" tabindex="0"><span class="text">Apparel</span><i class="glyphicon glyphicon-ok icon-ok check-mark"></i></a></li><li rel="2"><a style="" class="sub-category" tabindex="0"><span class="text">Computers</span><i class="glyphicon glyphicon-ok icon-ok check-mark"></i></a></li><li rel="3"><a style="" class="sub-category" tabindex="0"><span class="text">Nature</span><i class="glyphicon glyphicon-ok icon-ok check-mark"></i></a></li><li rel="4"><a style="" class="" tabindex="0"><span class="text">Tourism</span><i class="glyphicon glyphicon-ok icon-ok check-mark"></i></a></li><li rel="5"><a style="" class="" tabindex="0"><span class="text">Restaurant &amp; Bars</span><i class="glyphicon glyphicon-ok icon-ok check-mark"></i></a></li><li rel="6"><a style="" class="sub-category" tabindex="0"><span class="text">Bars</span><i class="glyphicon glyphicon-ok icon-ok check-mark"></i></a></li><li rel="7"><a style="" class="sub-category" tabindex="0"><span class="text">Vegetarian</span><i class="glyphicon glyphicon-ok icon-ok check-mark"></i></a></li><li rel="8"><a style="" class="sub-category" tabindex="0"><span class="text">Steak &amp; Grill</span><i class="glyphicon glyphicon-ok icon-ok check-mark"></i></a></li><li rel="9"><a style="" class="" tabindex="0"><span class="text">Sports</span><i class="glyphicon glyphicon-ok icon-ok check-mark"></i></a></li><li rel="10"><a style="" class="sub-category" tabindex="0"><span class="text">Cycling</span><i class="glyphicon glyphicon-ok icon-ok check-mark"></i></a></li><li rel="11"><a style="" class="sub-category" tabindex="0"><span class="text">Water Sports</span><i class="glyphicon glyphicon-ok icon-ok check-mark"></i></a></li><li rel="12"><a style="" class="sub-category" tabindex="0"><span class="text">Winter Sports</span><i class="glyphicon glyphicon-ok icon-ok check-mark"></i></a></li></ul></div></div>
                </div>
                <!-- /.form-group -->
                <div class="form-group" style="width: initial;">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                </div>
                <!-- /.form-group -->
            </div>
        </form>
        <!-- /.main-search -->
    </div>
    <!-- /.container -->
</div>


<script>
 //   $('select').selectpicker('render');
</script>

<!--[if lte IE 9]>
<script type="text/javascript" src="assets/js/ie-scripts.js"></script>
<![endif]-->

</div>
                <!-- /.search-bar -->
            </section>