@include('layouts.header')
<div id="page-canvas">
	@include('layouts.sub_header')
    @yield('content')
</div>
@include('layouts.footer')
        
