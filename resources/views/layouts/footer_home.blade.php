<footer id="page-footer">
            <div class="inner">
                <div class="footer-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <!--New Items-->
                                <section>
                                    <img src="{{ asset(null) }}assets/img/pasaran.png" style="width: 200px;margin-top: 10px;" alt="Pasaran Logo">
                                    <div class="wrapper">
                                      pasaran.com adalah situs pemasaran kebutuhan apapun dimanapun anda berada. Situs pemasaran No.1 di Indonesia ini memberikan banyak kemudahan terutama untuk pencarian barang, kendaraan, properti, pekerjaan, jasa, dan event disekitar anda.
                                    </div>
                                    <a href="{{ asset(null) }}about-us.html" class="read-more icon-readmore">Baca Selengkapnya</a>
                                </section>
                            </div>
                                <!--/.Social-media-->
                            <div class="col-md-2 col-sm-2">
                                <section>
                                        <h2><strong>Follow us</strong></h2>
                                        <a href="{{ asset(null) }}#" class="social-button"><i class="fa fa-twitter twitter"></i>Twitter</a>
                                        <a href="{{ asset(null) }}#" class="social-button"><i class="fa fa-facebook fb"></i>Facebook</a>
                                        <a href="{{ asset(null) }}#" class="social-button"><i class="fa fa-google gplus"></i>Google</a>
                                </section>
                            </div>
                                <!--/.End-social-media-->
                            <div class="col-md-2 col-sm-2">
                                <!--Recent Reviews-->
                                <section>
                                    <h2><strong>Navigasi</strong></h2>
                                    <a href="{{ asset(null) }}item-detail.html" class="small"><h3>FAQ</h3></a>
                                    <a href="{{ asset(null) }}blog-listing.html" class="small"><h3>Blog</h3></a>
                                    <a href="{{ asset(null) }}about-us.html" class="small"><h3>Tentang Kami</h3></a>
                                    <a href="{{ asset(null) }}terms-conditions.html" class="small"><h3>Syarat dan Ketentuan</h3></a>
                                </section>
                                <!--end Recent Reviews-->
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <section>
                                    <h2><strong>Tentang Kami</strong></h2>
                                    <address>
                                        <div>Kencana Tower Lt 7</div>
                                        <div>Business Park Kebon Jeruk</div>
                                        <div>Jl. Meruya Ilir Raya No. 88, Kembangan</div>
                                        <div>Jakarta Barat, 11620</div>
                                        <figure>
                                            <div class="info">
                                                <i class="fa fa-phone"></i>
                                                <span>(021)30061568</span>
                                            </div>
                                        </figure>
                                    </address>
                                    <a href="{{ asset(null) }}contact.html" class="btn framed icon">Hubungi Kami<i class="fa fa-angle-right"></i></a>
                                </section>
                            </div>
                            <!--/.col-md-4-->
                        </div>
                        <!--/.row-->
                    </div>
                    <!--/.container-->
                </div>
                <!--/.footer-top-->
                <div class="footer-bottom">
                    <div class="container">
                        <span class="left">Copyright © 2016 Pasaran.com. All Rights Reserved</span>
                            <span class="right">
                                <a href="{{ asset(null) }}#page-top" class="to-top roll"><i class="fa fa-angle-up"></i></a>
                            </span>
                    </div>
                </div>
                <!--/.footer-bottom-->
            </div>
        </footer>
        <!--end Page Footer-->
    </div>
    <!-- end Inner Wrapper -->
</div>
<!-- end Outer Wrapper-->
<script type="text/javascript">
    function asset()
    {
        return '{{ asset(null) }}';
    }
</script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/jquery-2.1.0.min.js"></script>
<script src="{{ og()->assetUrl }}sweetalert/dist/sweetalert.min.js"></script>

@yield('script')

@if(Session::has('globalMessage'))
    <script type="text/javascript">
        swal('success','{{ Session::get('globalMessage') }}','success');
    </script>
@endif


<script type="text/javascript" src="{{ asset(null) }}assets/js/before.load.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/smoothscroll.js"></script>
<?php /*
        <script type="text/javascript" src="{{ asset(null) }}assets/js/bootstrap-select.min.js"></script>
*/ ?>
<script type="text/javascript" src="{{ asset(null) }}assets/js/jquery.hotkeys.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/jquery.nouislider.all.min.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/custom.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/maps.js"></script>
<script>
    $(window).load(function(){
        var rtl = false; // Use RTL
        initializeOwl(rtl);
    });

    autoComplete();

    

</script>

<!--[if lte IE 9]>
<script type="text/javascript" src="{{ asset(null) }}assets/js/ie-scripts.js"></script>
<![endif]-->
</body>
</html>