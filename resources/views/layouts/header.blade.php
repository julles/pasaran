<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="{{ asset(null) }}assets/fonts/font-awesome.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ asset(null) }}assets/bootstrap/css/bootstrap.css" type="text/css">
    <?php /* <link rel="stylesheet" href="{{ asset(null) }}assets/css/bootstrap-select.min.css" type="text/css"> */ ?>
    <link rel="stylesheet" href="{{ asset(null) }}assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="{{ asset(null) }}assets/css/dropzone.css" type="text/css">
    <link rel="stylesheet" href="{{ asset(null) }}assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="{{ asset(null) }}assets/css/user.style.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ og()->assetUrl }}sweetalert/dist/sweetalert.css">
    <title>Pasaran.com - Situs Pemasaran No.1 Di Indonesia</title>

</head>
    
<body onunload="" class="page-subpage page-sign-in navigation-off-canvas" id="page-top">

<!-- Outer Wrapper-->
<div id="outer-wrapper">
    <!-- Inner Wrapper -->
    <div id="inner-wrapper">
        <!-- Navigation-->
        <div class="header">
            <div class="wrapper">
                <div class="brand">
                    <a href="{{ url('/') }}"><img src="{{ asset(null) }}assets/img/pasaran.png" class="logo" alt="Pasaran Logo">
                    <span class="tagline"> Situs Pemasaran No.1 di Indonesia </span>
                    </a>
                </div>
                <nav class="navigation-items">
                    <div class="wrapper">
                        <ul class="user-area">
                        @if(Auth::guard('member')->check())
                            <li><a href="{{ url('sign/out') }}"><i class="fa fa-sign-out fa-1x pull-left"></i><strong>Keluar</strong></a></li>
                        @else
                            <li><a href="{{ url('sign/login') }}"><i class="fa fa-user fa-1x pull-left"></i><strong>Akun Saya</strong></a></li>
                        @endif    
                        </ul>
                        <a href="{{ url('pasang-iklan') }}" class="submit-item">
                            <div class="icon">
                                <i class="fa fa-plus fa-2x pull-left"></i>Pasang Iklan
                            </div>

                        </a>
<!--                    <div class="toggle-navigation">
                            <div class="icon">
                                <div class="line"></div>
                                <div class="line"></div>
                                <div class="line"></div>
                            </div>
                        </div> -->
                    </div>
                </nav>
            </div>
        </div>