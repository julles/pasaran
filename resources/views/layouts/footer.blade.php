<footer id="page-footer">
            <div class="inner">
                <div class="footer-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <!--New Items-->
                                <section>
                                    <img src="{{ asset(null) }}assets/img/pasaran.png" style="width: 200px;" alt="Pasaran Logo">
                                    <div class="wrapper">
                                      pasaran.com adalah situs pemasaran kebutuhan apapun dimanapun anda berada. Situs pemasaran No.1 di Indonesia ini memberikan banyak kemudahan terutama untuk pencarian barang, kendaraan, properti, pekerjaan, jasa, dan event disekitar anda.
                                    </div>
                                    <a href="about-us.html" class="read-more icon">Baca Selengkapnya</a>
                                </section>
                            </div>
                                <!--/.Social-media-->
                            <div class="col-md-2 col-sm-2">
                                <section>
                                        <h2><strong>Follow us</strong></h2>
                                        <a href="#" class="social-button"><i class="fa fa-twitter twitter"></i>Twitter</a>
                                        <a href="#" class="social-button"><i class="fa fa-facebook fb"></i>Facebook</a>
                                        <a href="#" class="social-button"><i class="fa fa-google gplus"></i>Google</a>
                                    </div>
                                </section>
                                <!--/.End-social-media-->
                            <div class="col-md-2 col-sm-2">
                                <!--Recent Reviews-->
                                <section>
                                    <h2><strong>Navigasi</strong></h2>
                                    <a href="item-detail.html" class="small"><h3>FAQ</h3></a>
                                    <a href="blog-listing.html" class="small"><h3>Blog</h3></a>
                                    <a href="about-us.html" class="small"><h3>Tentang Kami</h3></a>
                                    <a href="terms-conditions.html" class="small"><h3>Syarat dan Ketentuan</h3></a>
                                </section>
                                <!--end Recent Reviews-->
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <section>
                                    <h2><strong>Tentang Kami</strong></h2>
                                    <address>
                                        <div>Kencana Tower Lt 7</div>
                                        <div>Business Park Kebon Jeruk</div>
                                        <div>Jl. Meruya Ilir Raya No. 88, Kembangan</div>
                                        <div>Jakarta Barat, 11620</div>
                                        <figure>
                                            <div class="info">
                                                <i class="fa fa-phone"></i>
                                                <span>(021)30061568</span>
                                            </div>
                                        </figure>
                                    </address>
                                    <a href="contact.html" class="btn framed icon">Hubungi Kami<i class="fa fa-angle-right"></i></a>
                                </section>
                            </div>
                            <!--/.col-md-4-->
                        </div>
                        <!--/.row-->
                    </div>
                    <!--/.container-->
                </div>
                <!--/.footer-top-->
                <div class="footer-bottom">
                    <div class="container">
                        <span class="left">Copyright © 2016 Pasaran.com. All Rights Reserved</span>
                            <span class="right">
                                <a href="#page-top" class="to-top roll"><i class="fa fa-angle-up"></i></a>
                            </span>
                    </div>
                </div>
                <!--/.footer-bottom-->
            </div>
        </footer>
        <!--end Page Footer-->
    </div>
    <!-- end Inner Wrapper -->
</div>
<!-- end Outer Wrapper-->

<script type="text/javascript">
    function asset()
    {
        return '{{ asset(null) }}';
    }

    function public_path()
    {
        return '{{ public_path(null) }}';
    }

</script>

<script type="text/javascript" src="{{ asset(null) }}assets/js/jquery-2.1.0.min.js"></script>
<script src="{{ og()->assetUrl }}sweetalert/dist/sweetalert.min.js"></script>

@yield('script')

@if(Session::has('globalMessage'))
    <script type="text/javascript">
        swal('success','{{ Session::get('globalMessage') }}','success');
    </script>
    <?php Session::forget('globalMessage'); ?>
@endif

@if(Session::has('globalMessageError'))
    <script type="text/javascript">
        swal('error','{{ Session::get('globalMessageError') }}','error');
    </script>
@endif

<script type="text/javascript" src="{{ asset(null) }}assets/js/before.load.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/jquery-ui.min.js"></script>
<?php /*    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDypvijZTcJJQHaIySiOBtU7qYlbH2_hOE&libraries=places&callback=initMap" async defer></script>
*/ ?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;libraries=places"></script>

<script type="text/javascript" src="{{ asset(null) }}assets/js/richmarker-compiled.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/smoothscroll.js"></script>
<?php /*         
        <script type="text/javascript" src="{{ asset(null) }}assets/js/bootstrap-select.min.js"></script>
*/ ?>
<script type="text/javascript" src="{{ asset(null) }}assets/js/icheck.min.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/jquery.hotkeys.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/dropzone.min.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/jquery.ui.timepicker.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/jquery.nouislider.all.min.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/smoothscroll.js"></script>
<script type="text/javascript" src="{{ asset(null) }}assets/js/custom.js"></script> 
<script type="text/javascript" src="{{ asset(null) }}assets/js/maps.js"></script> 


@yield('script_bottom')
<!--[if lte IE 9]>
<script type="text/javascript" src="{{ asset(null) }}assets/js/ie-scripts.js"></script>
<![endif]-->
</body>
</html>