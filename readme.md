# Pasaran Development Main Repo

##### Notes : The project still development but you can try this.


## How to Install ?

Clone Source Code

Setting your connection database in .env file

example
```sh
DB_HOST=127.0.0.1
DB_DATABASE=capsule
DB_USERNAME=root
DB_PASSWORD=yourpassword
```

Install composer depedencies

```sh
composer install
```

install capsule to completion

```sh
php artisan capsule:install
```

open your application in browser

Login User

|  Username  |      Password      |  Role |
|:--------:|:-------------:|------:|
|reza |  reza123 | Super Admin |
| admin |    admin123   |  Adminsitrator |

Finish

# Powered By : http://muhamadreza.id
