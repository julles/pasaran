<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIklanCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iklan_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->string('title');
            $table->string('slug',300);
            $table->timestamps();
        });

        \DB::table('iklan_categories')->insert([
                'id'            => 1,
                'parent_id'     => 0,
                'title'         => 'Music',
        ]);

        \DB::table('iklan_categories')->insert([
            'parent_id'     => 1,
            'title'         => 'Music',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('iklan_categories');
    }
}
