<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIklansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iklans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iklan_category_id')->unsigned();
            $table->integer('member_id')->unsigned();
            $table->string('judul');
            $table->string('foto');
            $table->text('deskripsi');
            $table->string('provinsi');
            $table->string('kota');
            $table->decimal('longitude');
            $table->decimal('lattitude');
            $table->string('slug',300);
            $table->timestamps();
        
            $table->foreign('iklan_category_id')->references('id')
                ->on('iklan_categories')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('member_id')->references('id')
                ->on('members')
                ->onUpdate('cascade')
                ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('iklans');
    }
}
