<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->string('pin_bb',10);
            $table->enum('whatsapp',['y','n']);
            $table->string('password');
            $table->string('photo');
            $table->string('slug')->index();
            $table->enum('newslater',['y','n']);
            $table->enum('status',['active','inactive','confirmation']);
            $table->string('google_key');
            $table->string('facebook_key');
            $table->string('twitter_key');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('members');
    }
}
