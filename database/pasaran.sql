/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : pasaran

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-04-04 16:21:45
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `actions`
-- ----------------------------
DROP TABLE IF EXISTS `actions`;
CREATE TABLE `actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `actions_code_index` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of actions
-- ----------------------------
INSERT INTO actions VALUES ('1', 'index', 'Index', null, null);
INSERT INTO actions VALUES ('2', 'create', 'Create', null, null);
INSERT INTO actions VALUES ('3', 'update', 'Update', null, null);
INSERT INTO actions VALUES ('6', 'view', 'View', '2016-02-28 17:10:11', '2016-02-28 17:10:11');
INSERT INTO actions VALUES ('7', 'delete', 'Delete', '2016-02-28 17:40:33', '2016-02-28 17:40:33');

-- ----------------------------
-- Table structure for `cruds`
-- ----------------------------
DROP TABLE IF EXISTS `cruds`;
CREATE TABLE `cruds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('men','woman') COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of cruds
-- ----------------------------
INSERT INTO cruds VALUES ('1', 'Dr. Jackeline Veum', 'Frami, Windler and Glover', 'woman', '', null, null, '');
INSERT INTO cruds VALUES ('2', 'Dorris Davis', 'Huel, Green and Steuber', 'men', '', null, null, '');
INSERT INTO cruds VALUES ('3', 'Prof. Mariana Sanford', 'Kassulke, O\'Keefe and Schuster', 'men', '', null, null, '');
INSERT INTO cruds VALUES ('5', 'Dr. Khalil Stoltenberg', 'Bartell-Witting', 'men', '', null, null, '');
INSERT INTO cruds VALUES ('6', 'Karianne Kertzmann', 'Smith, Prosacco and Koch', 'woman', '', null, null, '');
INSERT INTO cruds VALUES ('7', 'Alec Lemke', 'Roberts Inc', 'woman', '', null, null, '');
INSERT INTO cruds VALUES ('8', 'Prof. Kristin Collier DDS', 'Yundt Ltd', 'men', '', null, null, '');
INSERT INTO cruds VALUES ('9', 'Ms. Alexandra Witting', 'Gleason Ltd', 'woman', '', null, null, '');
INSERT INTO cruds VALUES ('10', 'Anastasia Satterfield', 'Olson, DuBuque and Hettinger', 'men', '', null, null, '');
INSERT INTO cruds VALUES ('11', 'tes', 'tes', 'men', 'm1p0Ty9BEt20160301181504.jpg', '2016-03-01 18:15:04', '2016-03-01 18:15:04', '');

-- ----------------------------
-- Table structure for `iklans`
-- ----------------------------
DROP TABLE IF EXISTS `iklans`;
CREATE TABLE `iklans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `iklan_category_id` int(10) unsigned NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8_unicode_ci NOT NULL,
  `provinsi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kota` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `lattitude` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('active','in_active','not_approve') COLLATE utf8_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `iklans_iklan_category_id_foreign` (`iklan_category_id`),
  KEY `iklans_member_id_foreign` (`member_id`),
  CONSTRAINT `iklans_iklan_category_id_foreign` FOREIGN KEY (`iklan_category_id`) REFERENCES `iklan_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `iklans_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of iklans
-- ----------------------------
INSERT INTO iklans VALUES ('5', '3', '1', 'CD MUSE OKE', '11N9yvxDBb20160319023855-iklan.jpg', 'lorep ipsum daule okrayse', 'jakarta', 'bogor', '1.00', '0.00', '5-cd-muse-oke', 'active', '0', '2016-03-19 02:38:56', '2016-04-02 15:58:10');
INSERT INTO iklans VALUES ('6', '1', '1', 'Muhamad Rezas', 'zY4CwMXLju20160330223559-iklan.jpg', 'okelah kalo begitoh', 'jakarga', 'hoseh habuse', '106.749899', '-6.591073', '6-muhamad-reza', 'active', '10000', '2016-03-19 02:44:33', '2016-04-02 20:56:06');

-- ----------------------------
-- Table structure for `iklan_categories`
-- ----------------------------
DROP TABLE IF EXISTS `iklan_categories`;
CREATE TABLE `iklan_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of iklan_categories
-- ----------------------------
INSERT INTO iklan_categories VALUES ('1', '0', 'Music', '', null, null);
INSERT INTO iklan_categories VALUES ('2', '1', 'CD', '', null, null);
INSERT INTO iklan_categories VALUES ('3', '2', 'MUSE', '', null, null);
INSERT INTO iklan_categories VALUES ('4', '1', 'Matt bellamy', '', '2016-03-20 02:54:41', '2016-03-20 03:08:29');

-- ----------------------------
-- Table structure for `members`
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pin_bb` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `whatsapp` enum('y','n') COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `newslater` enum('y','n') COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('active','inactive','confirmation') COLLATE utf8_unicode_ci NOT NULL,
  `google_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `twitter_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `members_slug_index` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO members VALUES ('1', 'reza ar', '12345', 'ultramantigar@gmail.com', '1234565', 'y', '$2y$10$cXJO/ruQ.ZXSX4e53hD6TOjk.JN9kNKOYdrsgcjwnan80EccuYnZq', '', '', 'n', 'active', '', '', '', '2016-03-17 15:32:29', '2016-03-17 15:33:38', '');

-- ----------------------------
-- Table structure for `member_histories`
-- ----------------------------
DROP TABLE IF EXISTS `member_histories`;
CREATE TABLE `member_histories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) unsigned DEFAULT NULL,
  `status` enum('login','logout') DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_member_histories_to_members` (`member_id`),
  CONSTRAINT `fk_member_histories_to_members` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of member_histories
-- ----------------------------
INSERT INTO member_histories VALUES ('1', '1', 'login', null, '2014-11-10 00:04:39', '2016-04-03 00:04:39');

-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `permalink` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menus_parent_id_index` (`parent_id`),
  KEY `menus_permalink_index` (`permalink`),
  KEY `menus_controller_index` (`controller`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO menus VALUES ('1', '0', 'Dashboard', 'dashboard', 'fa fa-dashboard', 'Backend\\DashboardController', '0', null, null);
INSERT INTO menus VALUES ('2', '0', 'User', '#', 'fa fa-user', '#', '1', null, null);
INSERT INTO menus VALUES ('3', '0', 'Sandbox', '#', 'fa fa-caret-square-o-right', '#', '99', null, null);
INSERT INTO menus VALUES ('4', '3', 'Crud Example', 'crud-example', '', 'Backend\\CrudController', '1', null, null);
INSERT INTO menus VALUES ('5', '2', 'Role ', 'role', '', 'Backend\\RoleController', '1', null, null);
INSERT INTO menus VALUES ('6', '2', 'User', 'user', '', 'Backend\\UserController', '1', null, null);
INSERT INTO menus VALUES ('7', '3', 'Action Management', 'action-management', '', 'Backend\\ActionController', '2', null, null);
INSERT INTO menus VALUES ('8', '3', 'Menu Management', 'menu-management', '', 'Backend\\MenuController', '3', null, null);
INSERT INTO menus VALUES ('9', '0', 'Iklan', '#', 'fa fa-file', '#', '0', '2016-03-20 02:14:39', '2016-03-20 02:14:39');
INSERT INTO menus VALUES ('10', '9', 'Category', 'category', '', 'Backend\\IklanCategoryController', '1', '2016-03-20 02:21:38', '2016-03-20 02:21:38');
INSERT INTO menus VALUES ('11', '9', 'Management Iklan', 'management-iklan', '', 'Backend\\IklanController', '2', '2016-03-22 00:44:27', '2016-03-22 00:44:27');

-- ----------------------------
-- Table structure for `menu_actions`
-- ----------------------------
DROP TABLE IF EXISTS `menu_actions`;
CREATE TABLE `menu_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned NOT NULL,
  `action_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_actions_menu_id_foreign` (`menu_id`),
  KEY `menu_actions_action_id_foreign` (`action_id`),
  CONSTRAINT `menu_actions_action_id_foreign` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menu_actions_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of menu_actions
-- ----------------------------
INSERT INTO menu_actions VALUES ('19', '5', '1', '2016-02-28 17:40:43', '2016-02-28 17:40:43');
INSERT INTO menu_actions VALUES ('20', '5', '2', '2016-02-28 17:40:43', '2016-02-28 17:40:43');
INSERT INTO menu_actions VALUES ('21', '5', '3', '2016-02-28 17:40:43', '2016-02-28 17:40:43');
INSERT INTO menu_actions VALUES ('22', '5', '6', '2016-02-28 17:40:43', '2016-02-28 17:40:43');
INSERT INTO menu_actions VALUES ('23', '5', '7', '2016-02-28 17:40:43', '2016-02-28 17:40:43');
INSERT INTO menu_actions VALUES ('24', '6', '1', '2016-02-28 17:40:55', '2016-02-28 17:40:55');
INSERT INTO menu_actions VALUES ('25', '6', '2', '2016-02-28 17:40:55', '2016-02-28 17:40:55');
INSERT INTO menu_actions VALUES ('26', '6', '3', '2016-02-28 17:40:55', '2016-02-28 17:40:55');
INSERT INTO menu_actions VALUES ('27', '6', '7', '2016-02-28 17:40:55', '2016-02-28 17:40:55');
INSERT INTO menu_actions VALUES ('28', '1', '1', '2016-02-28 17:41:16', '2016-02-28 17:41:16');
INSERT INTO menu_actions VALUES ('42', '4', '1', '2016-02-28 17:42:12', '2016-02-28 17:42:12');
INSERT INTO menu_actions VALUES ('43', '4', '2', '2016-02-28 17:42:12', '2016-02-28 17:42:12');
INSERT INTO menu_actions VALUES ('44', '4', '3', '2016-02-28 17:42:12', '2016-02-28 17:42:12');
INSERT INTO menu_actions VALUES ('45', '4', '7', '2016-02-28 17:42:12', '2016-02-28 17:42:12');
INSERT INTO menu_actions VALUES ('46', '7', '1', '2016-02-28 17:42:43', '2016-02-28 17:42:43');
INSERT INTO menu_actions VALUES ('47', '7', '2', '2016-02-28 17:42:43', '2016-02-28 17:42:43');
INSERT INTO menu_actions VALUES ('48', '7', '3', '2016-02-28 17:42:44', '2016-02-28 17:42:44');
INSERT INTO menu_actions VALUES ('49', '7', '7', '2016-02-28 17:42:44', '2016-02-28 17:42:44');
INSERT INTO menu_actions VALUES ('50', '8', '1', '2016-02-28 17:42:55', '2016-02-28 17:42:55');
INSERT INTO menu_actions VALUES ('51', '8', '2', '2016-02-28 17:42:55', '2016-02-28 17:42:55');
INSERT INTO menu_actions VALUES ('52', '8', '3', '2016-02-28 17:42:55', '2016-02-28 17:42:55');
INSERT INTO menu_actions VALUES ('53', '8', '6', '2016-02-28 17:42:55', '2016-02-28 17:42:55');
INSERT INTO menu_actions VALUES ('54', '8', '7', '2016-02-28 17:42:55', '2016-02-28 17:42:55');
INSERT INTO menu_actions VALUES ('55', '10', '1', '2016-03-20 02:22:03', '2016-03-20 02:22:03');
INSERT INTO menu_actions VALUES ('56', '10', '2', '2016-03-20 02:22:03', '2016-03-20 02:22:03');
INSERT INTO menu_actions VALUES ('57', '10', '3', '2016-03-20 02:22:03', '2016-03-20 02:22:03');
INSERT INTO menu_actions VALUES ('58', '10', '7', '2016-03-20 02:22:03', '2016-03-20 02:22:03');
INSERT INTO menu_actions VALUES ('59', '11', '1', '2016-03-22 00:44:59', '2016-03-22 00:44:59');
INSERT INTO menu_actions VALUES ('60', '11', '6', '2016-03-22 00:44:59', '2016-03-22 00:44:59');
INSERT INTO menu_actions VALUES ('61', '11', '7', '2016-03-22 00:44:59', '2016-03-22 00:44:59');

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO migrations VALUES ('2016_02_17_182212_create_roles_table', '1');
INSERT INTO migrations VALUES ('2016_02_17_182518_create_users_table', '1');
INSERT INTO migrations VALUES ('2016_02_17_183316_create_menus_table', '1');
INSERT INTO migrations VALUES ('2016_02_17_184125_update_users_table_add_facebook', '1');
INSERT INTO migrations VALUES ('2016_02_19_173350_create_cruds_table', '1');
INSERT INTO migrations VALUES ('2016_02_21_134953_create_actions_table', '1');
INSERT INTO migrations VALUES ('2016_02_23_171459_update_users_table_add_remember_token', '1');
INSERT INTO migrations VALUES ('2016_02_25_185321_create_menu_actions_table', '1');
INSERT INTO migrations VALUES ('2016_02_28_183253_create_rights_table', '1');
INSERT INTO migrations VALUES ('2016_03_09_163233_update_table_crud_cruds_add_description', '1');
INSERT INTO migrations VALUES ('2016_03_16_181931_create_members_table', '1');
INSERT INTO migrations VALUES ('2016_03_17_144558_add_some_fields_in_members_table', '1');
INSERT INTO migrations VALUES ('2016_03_17_150732_create_iklan_categories_table', '1');
INSERT INTO migrations VALUES ('2016_03_17_151140_create_iklans_table', '1');
INSERT INTO migrations VALUES ('2016_03_17_155552_add_field_harga_in_iklans_table', '2');
INSERT INTO migrations VALUES ('2016_03_17_163905_add_field_status_in_iklans_table', '3');
INSERT INTO migrations VALUES ('2016_03_19_024206_add_field_harga_in_iklans_table', '4');

-- ----------------------------
-- Table structure for `rights`
-- ----------------------------
DROP TABLE IF EXISTS `rights`;
CREATE TABLE `rights` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `menu_action_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rights_menu_action_id_foreign` (`menu_action_id`),
  KEY `rights_role_id_foreign` (`role_id`),
  CONSTRAINT `rights_menu_action_id_foreign` FOREIGN KEY (`menu_action_id`) REFERENCES `menu_actions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rights_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of rights
-- ----------------------------
INSERT INTO rights VALUES ('45', '42', '2', '2016-03-01 18:05:49', '2016-03-01 18:05:49');
INSERT INTO rights VALUES ('46', '43', '2', '2016-03-01 18:05:49', '2016-03-01 18:05:49');
INSERT INTO rights VALUES ('47', '44', '2', '2016-03-01 18:05:49', '2016-03-01 18:05:49');
INSERT INTO rights VALUES ('48', '45', '2', '2016-03-01 18:05:49', '2016-03-01 18:05:49');
INSERT INTO rights VALUES ('49', '46', '2', '2016-03-01 18:05:49', '2016-03-01 18:05:49');
INSERT INTO rights VALUES ('76', '42', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('77', '43', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('78', '44', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('79', '45', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('80', '46', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('81', '47', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('82', '48', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('83', '49', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('84', '50', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('85', '51', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('86', '52', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('87', '53', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('88', '54', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('89', '19', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('90', '20', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('91', '21', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('92', '22', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('93', '23', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('94', '24', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('95', '25', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('96', '26', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('97', '27', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('98', '55', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('99', '56', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('100', '57', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('101', '58', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('102', '59', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('103', '60', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');
INSERT INTO rights VALUES ('104', '61', '1', '2016-03-22 00:45:22', '2016-03-22 00:45:22');

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO roles VALUES ('1', 'Super Admin', null, null);
INSERT INTO roles VALUES ('2', 'Administrator', '2016-02-21 12:39:38', '2016-02-21 12:39:38');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `twitter_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('y','n') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `users_role_id_foreign` (`role_id`),
  KEY `users_username_index` (`username`),
  KEY `users_email_index` (`email`),
  KEY `users_facebook_id_index` (`facebook_id`),
  KEY `users_twitter_id_index` (`twitter_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO users VALUES ('1', '1', '', '', 'prana', '$2y$10$RE9g3osmblBV9bO8OqLlaOV1BRErrXhnBwcdciZNwi2XQ6KDvmlBW', 'Prana Jaya', 'prana@gmail.com', 'T0aM3XnkPK20160223163453.jpg', 'y', null, '2016-03-22 00:42:34', 'nZfArdLkdVCBzJT9BQiErwKXaiivUvgcXOgz2LA8L9t2YvqXj2eGtAcQR57J');
INSERT INTO users VALUES ('2', '2', '', '', 'admin', '$2y$10$IMLVHMIR83tbgGMVl1o0..QsW1/KuZDkWAB2ti3AVU.HXaITeC8by', 'Administrator', 'ultramantigar@gmail.com', 'Akm7NALs1q20160223171954.jpg', 'y', '2016-02-23 17:19:54', '2016-03-01 18:44:36', '1VUM5ICvZdCjWpv7CG6iXUxfewYGKPUS6dRLayaX8XihP2yBMYZXxPO43tAW');
