<?php
/**
 * Author : Muhamad Reza Abdul Rohim <reza.wikrama3@gmail.com>
 * 
 * CV Oblagio Berdaya
 * 
 * Helper Core Oblagio Class
 * 
 */
function og()
{
	return new App\Oblagio\Acme\Src\Oblagio;
}

function oblagioRandom()
{
	return og()->oblagioRandom();
}

function contentsPath($url="")
{
	return og()->contentsPath.$url;
}

function assetContents($url)
{
	return og()->assetUrl.'contents/'.$url;
}

function menuAttribute($permalink = "")
{
		return og()->getMenuAttribute($permalink);
}

function menuAttributeFind($id)
{
	return og()->getMenuAttributeFind($id);
}

function actionAttribute($permalink = "")
{
	return og()->getActionAttribute($permalink);
}

function user()
{
	return auth()->user();
}

function member()
{
	return auth()->guard('member')->user();
}

function carbonParse($parse,$format)
{
	return og()->carbon()->parse($parse)->format($format);
}

function oblagioHash($param)
{
	return md5(md5(md5(md5("Muhamad Reza Abdul Rohim!@#$%^&*()".$param))));
}

function ago($date)
{
	$created = new \Carbon\Carbon($date);
	
	$now = \Carbon\Carbon::now();
	
	$day = $now->diff($created)->days;
	
	if($day == 0)
	{
		return 'Today';
	}else{
		return $now->subDays($day)->diffForHumans();
	}
	
}

function agoIndo($date)
{
	$ago = ago($date);

	$array = ['Hari Ini','Kemarin','Minggu Ini',''];
}

function icon_status($status)
{
	if($status == 'not_approve')
	{
		$class = 'ribbon in-queue';
		$icon = 'fa fa-clock-o';
		
	}elseif($status == 'active'){
		$class = 'ribbon approved';
		$icon = 'fa fa-check';
	}elseif($status == 'in_active'){
		$class = 'ribbon in-non-aktif';
		$icon = 'fa fa-eye-slash';
	}

	$str = '<div class="'.$class.'">
	            <i class="'.$icon.'"></i>
	        </div>';

	return $str;
}

function injectModel($class)
{
	$model = "App\Models\\$class";

	return new $model;
}