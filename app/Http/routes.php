<?php
include __DIR__.'/oblagioRoutes.php';
Route::group(['middleware' => ['web']], function () {
	
	// authenticate

		Route::get('auth','Backend\AuthController@login');
		Route::post('auth','Backend\AuthController@postLogin');
		Route::get('auth/sign-out','Backend\AuthController@signOut');
		Route::get('auth/forgot-password','Backend\AuthController@forgotPassword');
		Route::post('auth/forgot-password','Backend\AuthController@postForgotPassword');	

	//
	
	Route::group(['middleware' => ['auth:member']] , function(){
		Route::controller('member' , 'MemberController');
	});

	// iklan
		Route::get('pasang-iklan' ,  'IklanController@pasangIklan');
		Route::post('post-pasang-iklan','IklanController@postPasangIklan');
		Route::get('iklan/{name}/{slugIklan}','IklanController@viewIklan');
	//
	

	Route::controller('sign','SignController');
	Route::controller('/','HomeController');
	
	
	

});


