<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Member;
use Validator;
use Auth;

class SignController extends Controller
{
	public $model;

	public function __construct(Member $model)
	{
		$this->model = $model;
    }

    public function getLogin()
    {
    	return view('sign.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        $credentials = [
            'email'     => $request->email,
            'password'  => $request->password,
            'status'    => 'active'
        ];

        if(!Auth::guard('member')->attempt($credentials))
        {
            return redirect()->back()->with('globalMessageError','Username or password wrong!')->withInput();
        }

        og()->memberAction([
            'member_id' => member()->id,
            'status'    => 'login',
        ]);

        return redirect('member')->with('globalMessage','Hello Again');
    }

    public function getDaftar()
    {
    	return view('sign.daftar');
    }

    public function postDaftar(Request $request)
    {
    	$model = $this->model;

    	$rules = [
    		'email'		=> 'required|email|unique:members',
    		'password'	=> 'required|min:8',
    		'confirm'	=> 'same:password',
    	];

    	$validation = Validator::make($request->all() , $rules);

    	if($validation->fails())
    	{
    		return redirect()->back()->withErrors($validation)->withInput();
    	}

    	$saveMember = $model->create([
    		'email'		=> $request->email,
    		'password'	=> \Hash::make($request->password),
    		'status'	=> 'confirmation',
    		'newslater'	=> (!empty($request->newslater)) ? 'y' :'n',
    	]);

    	$generateKey = oblagioHash($saveMember->id);

    	\Mail::send('sign.email_daftar', ['model' => $saveMember,'generateKey' => $generateKey], function ($m) use ($request) {
            $m->from('no-reply@pasaran.com');
			$m->to($request->email)->subject('Email Confirmation');
        });

        return redirect()->back()->withSuccess('Silahkan konfirmasi di email anda');
    }

    public function getConfirmation($id , $params)
    {
    	if(oblagioHash($id) == $params)
    	{
    		$model = $this->model->findOrFail($id);
    		
    		$model->update([
    			'status'	=> 'active',
    		]);
    		
    		$login = Auth::guard('member')->loginUsingid($id);
    		
    		return redirect('member')->with('globalMessage','Confirmation email success!');

    	}else{

    		abort(404);
    	}
    }

    public function getOut()
    {
        og()->memberAction([
            'member_id' => member()->id,
            'status'    => 'logout',
        ]);

        Auth::guard('member')->logout();

        return redirect('/');
    }
    
}
