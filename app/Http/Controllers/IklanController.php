<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Iklan;
use App\Models\IklanCategory;
use App\Models\IklanView;
use Auth;
use Image;

class IklanController extends Controller
{
	public $model;
	public $category;
    public $view;

	public function __construct(Iklan $model,IklanCategory $category,IklanView $view)
	{
		$this->model = $model;

		$this->category = $category;

        $this->view = $view;
	}

	public function childs($model,$no){
		
        $strip = function($no){
            if($no == 1)
            {
                //$class = "sub-category";
                $strip = ' - ';
            }elseif($no == 2){
                //$class = "sub-category-second";
                $strip = ' - - ';
            }else{
                //$class = "sub-category-third";
                $strip = ' - - - ';
            }

            return $strip;
        };

        $str = "";

        $no = $no;

        foreach($model->childs as $row)
		{
            $no++;

            $str .= "<option  value = '".$row->title."'>".$strip($no).$row->title."</option>";

			$str .= $this->childs($row,$no);
		}

		return $str;
	}

	public function categories($id = "")
	{
		$model = $this->category->select('id','title');

		$str = "";
		
        $no = 0;

		foreach($model->whereParentId(0)->get() as $row)
		{
            $no++;

			$str .= "<option value = '".$row->id."'>".$row->title."</option>";
			$str .= $this->childs($row,$no);
		}

		return $str;
	}

    public function pasangIklan()
    {
    	$categories = $this->categories();
        $model = $this->model;
    	return view('iklan.pasang' , compact('categories','model'));
    }

    public function postPasangIklan(Request $request)
    {
    	if(Auth::guard('member')->check())
    	{
    		return $this->postPasangIklanMember($request);
    	}
    }

    public function rules()
    {
    	$rules = [
    		'judul'				=> 'required',
    		'foto'				=> 'required|mimes:jpg,png,gif,jpeg,JPG',
    		'iklan_category_id'	=> 'required',
    		'provinsi'			=> 'required',
    		'kota'				=> 'required',
    		'terms'				=> 'required',
    	];

    	return $rules;
    }

    public function handleUpload($request)
    {
    	$foto = $request->file('foto');

    	$imageName = oblagioRandom().'-iklan.'.$foto->getClientOriginalExtension();

    	Image::make($foto)->save(public_path('contents/'.$imageName));

    	Image::make($foto)->resize(555,415)->save(public_path('contents/thumbnails/'.$imageName));

    	return $imageName;
    }

    public function postPasangIklanMember($request)
    {
    	$this->validate($request , $this->rules());

    	$authMember = Auth::guard('member')->user();

    	$inputs = $request->all();

    	$foto = $this->handleUpload($request);

    	$inputs['member_id']= $authMember->id;

    	$inputs['foto']=$foto;

        $inputs['status']='not_approve';

    	$inputs['iklan_category_id'] = $this->category->where('title',$request->iklan_category_id)->first()->id;

    	$model = $this->model->create($inputs);
        
        $slug = str_slug($model->id.'-'.$model->judul);

        $update = $this->model->find($model->id)->update(['slug' => $slug]);

    	return redirect('member/iklan-saya/menunggu-verifikasi')->with('globalMessage','Iklan telah disimpan,Tunggu approval dari admin');
    }

    public function handleViewUser($iklanId)
    {   
        $y = date('Y');
        $m = date('m');
        $d = date('d');

        $ip = request()->ip();
        $cek = $this->view->whereRaw('YEAR(created_at) = "'.$y.'"')
                
                ->whereRaw('MONTH(created_at) = "'.$m.'"')
                
                ->whereRaw('DAY(created_at) = "'.$d.'"')
                
                ->whereIklanId($iklanId)
                
                ->first();
        
        if(empty($cek->id))
        {
            $model = $this->view->create([
                    'iklan_id'  => $iklanId,
                    'ip'        => $ip,
            ]);
        }
    }

    public function viewIklan($user,$slugIklan)
    {
        $model = $this->model->whereSlug($slugIklan)->firstOrFail();
        $this->handleViewUser($model->id);
        return view('iklan.view_iklan',compact('model'));
    }

}
