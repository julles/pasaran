<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Member;
use App\Models\Iklan;
use App\Models\IklanCategory;
use Validator;
use Image;

class MemberController extends Controller
{
	public $model;

	public $authMember;

	public function __construct(Member $model,Iklan $iklan)
	{
		$this->model = $model;
        $this->iklan = $iklan;
		$this->authMember = Auth::guard('member')->user();
	}

    public function name()
    {
        $authMember = $this->authMember;

        if(empty($authMember->name))
        {
            $result = explode("@", $authMember->email);
            return $result[0];
        }else{
            return $authMember->name;
        }
    }

	public function getIndex()
    {
    	$authMember = $this->authMember;
        
        $name = $this->name();
    	
        return view('member.profile' ,['model' => $authMember , 'name' => $name]);
    }

    public function handleUpload($request)
    {
    	$photo =  $request->file('photo');

    	if(!empty($request->photo))
    	{
    		echo 'ok';
    	}else{
    		echo 'awe';
    	}
    }

    public function postIndex(Request $request)
    {
    	$model = $this->model->findOrFail($this->authMember->id);

    	$inputs = $request->all();

    	$rules = [
    		'name'	=> 'required',
    	];

    	$this->validate($request,$rules);

    	$inputs['whatsapp'] = (!empty($request->whatsapp)) ? 'y' : 'n';

    	$model->update($inputs);
    	
    	return redirect()->back()->with('globalMessage' , 'Data has been updated');
    }

    public function postChangePassword(Request $request)
    {
    	$model = $this->model->findOrFail($this->authMember->id);

    	$check = Auth::guard('member')->validate(['password' => $request->old_password]);
    	
    	if($check == false)
    	{
    		return redirect()->back()->with('globalMessageError' ,'Your Old Password wrong!');
    	}else{

	    	$validation = Validator::make($request->all() , ['password' => 'required|min:8','confirm' => 'same:password']);
	    	
	    	if($validation->fails())
	    	{
	    		return redirect('member')->withErrors($validation);
	    	}
	    }

	    $model->update(['password' => \Hash::make($request->password)]);

	    return redirect()->back()->with('globalMessage' , 'Password has been updated');
    }

    public function getIklanSaya($status = "")
    {
        $model = $this->iklan->whereMemberId($this->authMember->id)->orderBy('created_at','desc');

        if($status == '' || $status == 'all')
        {
            $model = $model;
        
        }elseif($status == 'aktif'){
        
            $model = $model->whereStatus('active');
        
        }elseif($status == 'menunggu-verifikasi'){
            
            $model = $model->whereStatus('not_approve');
        
        }elseif($status == 'tidak-aktif'){
            
            $model = $model->whereStatus('in_active');
        }else{
            abort(404);
        }

        $model = $model->get();

        return view('member.iklan_saya',[
            'model'     => $model,
            'name'      => $this->name(),
        ]);
    }

    public function getEditIklan($slug)
    {
        $iklan = new IklanController($this->iklan,new IklanCategory);
        $categories = $iklan->categories();
        if(Auth::guard('member')->check())
        {
            $model = $this->iklan->whereSlug($slug)->first();

            if(!empty($model->id))
            {
                 return view('member.edit_iklan',compact('model','categories'));
            }else{
                abort(404);
            }
        
        }else{
            abort(404);
        }
    }

    public function handleUploadIklan($request,$model)
    {
        if(!empty($model->foto))
        {
            @unlink(public_path("contents/".$model->foto));
            @unlink(public_path("contents/thumbnails/".$model->foto));
        }

        $foto = $request->file('foto');

        $imageName = oblagioRandom().'-iklan.'.$foto->getClientOriginalExtension();

        Image::make($foto)->save(public_path('contents/'.$imageName));

        Image::make($foto)->resize(555,415)->save(public_path('contents/thumbnails/'.$imageName));

        return $imageName;
    }

    public function rules()
    {
        $rules = [
            'judul'             => 'required',
            'foto'              => 'mimes:jpg,png,gif,jpeg,JPG',
            'iklan_category_id' => 'required',
            'provinsi'          => 'required',
            'kota'              => 'required',
            'terms'             => 'required',
        ];

        return $rules;
    }

    public function postEditIklan(Request $request , $slug)
    {
        $model = $this->iklan->whereSlug($slug)->first();

        if(empty($model->id))
        {
            abort(404);
        }

        $this->validate($request , $this->rules());

        $authMember = Auth::guard('member')->user();

        $inputs = $request->all();

        if(!empty($request->foto))
        {
            $foto = $this->handleUploadIklan($request,$model);
            $inputs['foto']=$foto;
        }

        $inputs['member_id']= $authMember->id;
        
        $inputs['iklan_category_id'] = $request->iklan_category_id;

        $model = $model->update($inputs);
        
        return redirect('member/iklan-saya/all')->with('globalMessage','Iklan telah di update');
    }
}
