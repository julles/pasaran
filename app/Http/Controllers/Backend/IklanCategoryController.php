<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Backend\CapsuleController;
use App\Models\IklanCategory;

class IklanCategoryController extends CapsuleController
{
	public $model;

    public function __construct(IklanCategory $model)
    {
    	parent::__construct();

    	$this->model = $model;
    }

    public function getIndex()
	{
		$model = $this->model;
		return view('oblagio.iklan_kategori.index',compact('model'));
	}

	public function categories($modelParent = "",$number)
	{

		if(!empty($modelParent))
		{
			$model = $modelParent->childs;
		}else{
			$model = $this->model->whereParentId(0)->get();
		}
		

		$arr = [];

		$strip = function($no){
			$str="";
			for($i=0;$i<$no;$i++)
			{
				$str.='-';
			}

			return $str;
		};

		$no = $number + 1;

		foreach($model as $row)
		{
			$arr[$row->id] = $strip($no).' '.$row->title;

			$arr += $this->categories($row,$no+1);
		}

		return $arr;
	}

	public function getCreate()
	{
		$model = $this->model;
		
		$categories = $this->categories("",0);

		return view('oblagio.iklan_kategori._form',compact('model','categories'));
	}

	public function postCreate(Request $request)
	{
		$this->validate($request , $this->model->rules());

		$this->model->create($request->all());

		return og()->flashSuccess('Data has been saved');	
	}

	public function getUpdate($id)
	{
		$model = $this->model->findOrFail($id);
		
		$categories = $this->categories("",0);

		return view('oblagio.iklan_kategori._form',compact('model','categories'));
	}

	public function postUpdate(Request $request , $id)
	{
		$this->validate($request , $this->model->rules());

		$this->model->findOrFail($id)->update($request->all());

		return og()->flashSuccess('Data has been updated');	
	}


	public function getDelete($id)
	{
		$model = $this->model->findOrFail($id);

		try
		{
			$model->delete();
			return og()->flashSuccess('Data has been deleted');	
		}catch(\Exception $e){
			return og()->flashError('Data data cannot be deleted');	
		}
	}
}
