<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Backend\CapsuleController;
use App\Models\Iklan;
use Table;
class IklanController extends CapsuleController
{
	public $model;

    public function __construct(Iklan $model)
    {
    	parent::__construct();
    	$this->model = $model;
    }

    public function getData($status = "")
	{
		if($status == "")
		{
			$status = "active";
		}

		$model = $this->model->select('iklans.id','members.name','iklans.status','iklans.judul')
			->join('members','members.id','=','iklans.member_id')
			->where('iklans.status',$status);

		$tables = Table::of($model)
			->addColumn('action',function($model){
					return og()->links($model->id , ['view','delete']);
    		})
    		->addColumn('status',function($model){
					switch ($model->status) {
						case 'active':
							return '<small class = "label label-success">Active</small>';
						break;
						case 'not_active':
							return '<small class = "label label-danger">Not Active</small>';
						break;
						case 'not_approve':
							return '<small class = "label label-warning">Pending</small>';
						break;
						default:
							return '<small class = "success">Active</small>';
						break;
					}
    		})
			->make(true);

		return $tables;
	}

	public function getIndex($status = "")
    {
		return view('oblagio.iklan.index',compact('status'));    	
    }

    public function getView($id)
    {
    	$model = $this->model->findOrFail($id);

    	return view('oblagio.iklan.view',compact('model'));
    }

    public function postView(Request $request,$id)
    {
    	$status =  $request->status;

    	$model = $this->model->findOrFail($id);

    	$model->update([
    		'status'	=> $status,
    	]);

    	return og()->flashSuccess('Data has been updated');
    }
}
