<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Repositories\Iklan;

class HomeController extends Controller
{
	public $iklan;

	public function __construct(Iklan $iklan)
	{
		$this->iklan = $iklan;
	}

    public function getIndex()
    {
    	$terbaru = $this->iklan->homeTerbaru();
    	$populers = $this->iklan->populer();
    	return view('home',compact('terbaru','populers'));
    }
}
