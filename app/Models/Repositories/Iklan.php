<?php namespace App\Models\Repositories;

use App\Models\Iklan as IklanMaster;

class Iklan extends IklanMaster
{
	public function newest()
	{
		$model =  $this->where('status','active')->orderBy('created_at','desc');
		
		return $model;
	}

	public function homeTerbaru()
	{
		return $this->newest()->limit(4)->get();
	}
	
	public function populer()
	{
		return $this->newest()->views()->count();
	}

	
}