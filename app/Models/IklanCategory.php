<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Iklan;

class IklanCategory extends Model
{
    protected $table = 'iklan_categories';

    public $guarded = [];

    public function iklans()
    {
    	return $this->hasMany(Iklan::class);
    }

    public function childs()
    {
    	return $this->hasMany(IklanCategory::class,'parent_id','id');
    }

    public function parent()
    {
    	return $this->belongsTo(Iklan::class,'parent_id');
    }

    public function rules()
    {
        return [
            'title'     => 'required'
        ];
    }
}
