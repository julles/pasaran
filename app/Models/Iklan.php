<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\IklanCategory;
use App\Models\Member;
use App\Models\IklanView;

class Iklan extends Model
{
    protected $table = 'iklans';

    public $guarded = ['terms'];

    public function category()
    {
    	return $this->belongsTo(IklanCategory::class ,'iklan_category_id');
    }

    public function member()
    {
    	return $this->belongsTo(Member::class,'member_id');
    }

    public function views()
    {
        return $this->hasMany(IklanView::class);
    }
}
