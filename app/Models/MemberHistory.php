<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberHistory extends Model
{
	protected $table = 'member_histories';

    public $guarded = [];
}
