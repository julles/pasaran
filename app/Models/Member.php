<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\MemberHistory;
class Member extends Authenticatable
{
    protected $table = 'members';

    public $guarded = [];

    public function rules()
    {
    	
    }

    public function histories()
    {
    	return $this->hasMany(MemberHistory::class);
    }

    public function lastAction()
    {
    	
    }

    public function lastLogin()
    {
    	$model = $this->histories()->orderBy('created_at','desc')->limit(1)->first();

    	if(!empty($model->id))
    	{
    		return ago($model->created_at);
    	}else{
    		return null;
    	}
    }
}
